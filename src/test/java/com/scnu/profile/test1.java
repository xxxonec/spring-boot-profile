package com.scnu.profile;

import com.alibaba.fastjson.JSON;
import com.feilong.core.bean.ConvertUtil;
import com.feilong.core.util.AggregateUtil;
import com.geccocrawler.gecco.request.HttpGetRequest;
import com.geccocrawler.gecco.request.HttpRequest;
import com.hankcs.hanlp.HanLP;
import com.scnu.profile.module.crawler.JournalDetailInfo;
import com.scnu.profile.module.scholat.service.PaperService;
import com.scnu.profile.module.scholat.service.UnitService;
import com.scnu.profile.module.system.service.RedisService;
import com.scnu.profile.mybatis.mapper.*;
import com.scnu.profile.mybatis.model.*;
import com.scnu.profile.module.scholat.service.AuthorService;
import com.scnu.profile.module.system.service.RoleService;
import com.scnu.profile.utils.ServiceUtils;
import com.scnu.profile.utils.AddressUtils;
import com.scnu.profile.utils.TreeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import redis.clients.jedis.Jedis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by pcmmm on 17/7/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class test1{
    @Autowired
    AuthorService authorService;
    @Autowired
    AuthorMapper authorMapper;
    @Autowired
    JournalMapper journalMapper;
    @Autowired
    PaperMapper paperMapper;
    @Autowired
    PaperService paperService;
    @Autowired
    UnitMapper unitMapper;
    @Autowired
    RoleService roleService;
    @Autowired
    UnitService unitService;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserRoleMapper userRoleMapper;
    @Autowired
    ResultDisMapper resultDisMapper;
    @Autowired
    ResultTrendMapper resultTrendMapper;
    @Autowired
    ResultTopMapper resultTopMapper;

    @Test
    public void convert2txt(){
        List<Paper> paperList  =ServiceUtils.getServiceUtils().getPaperService().findAll();
        List<Author> authorList  =ServiceUtils.getServiceUtils().getAuthorService().findAll();
        List<Unit> unitList  =ServiceUtils.getServiceUtils().getUnitService().findAll();
        List<Journal> journalList  =ServiceUtils.getServiceUtils().getJournalService().findAll();
        try {
            FileWriter paperWriter = new FileWriter("data/paper.txt",true);
            FileWriter authorWriter = new FileWriter("data/author.txt",true);
            FileWriter unitWriter = new FileWriter("data/unit.txt",true);
            FileWriter journalWriter = new FileWriter("data/journal.txt",true);
            for(int i = 0;i<paperList.size();i++) {
                paperWriter.write(paperList.get(i).toTxt());
            }
            for(int i = 0;i<authorList.size();i++) {
                authorWriter.write(authorList.get(i).toTxt());
            }
            for(int i = 0;i<unitList.size();i++) {
                unitWriter.write(unitList.get(i).toTxt());
            }
            for(int i = 0;i<journalList.size();i++) {
                journalWriter.write(journalList.get(i).toTxt());
            }
            paperWriter.close();
            authorWriter.close();
            unitWriter.close();
            journalWriter.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    @Test
    public void test() {
        //List<Paper> papers = authorService.getAllPaper("05966545");
        String summarys = authorService.getAllSummary("05966545");
        String keywords = authorService.getAllKeyword("05966545");
        List<String> keywordList = HanLP.extractPhrase(summarys, 10);
        String[] keys = keywords.split(",");
        List<String> keylist = ConvertUtil.toList(keys);
        //Map<String, Integer> map = AggregateUtil.groupCount(keylist,"name");
        //
        Map map = new HashMap();

        for (String temp : keylist) {
            Integer count = (Integer) map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }
        //System.out.println("\nMap排序-以key排序");
        //String jsonString = JSON.toJSONString(SortUtil.sortMapByValueDesc(map));
        List<Map.Entry<String,Integer>> list =
                new ArrayList<Map.Entry<String,Integer>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue() - o1.getValue());
            }
        });
        System.out.println(keywordList.toString());
        System.out.println("====================================================");
        System.out.println(list);
    }

    @Test
    public void test2(){
        List<Author> papers = ServiceUtils.getServiceUtils().getAuthorService().findAll();
        System.out.println("list:"+papers.toString());
    }

    @Test
    public void test3(){
        List<Unit> units = unitMapper.getAllNullAddress();
        for(Unit u : units){
            try {
                Map<String,String> map = AddressUtils.getAddress(u.getName());
                if(!map.isEmpty()){
                    u.setCategory(map.get("category"));
                    u.setAddress(map.get("address"));
                    u.setCity(map.get("city"));
                    u.setDistrict(map.get("district"));
                    u.setLatLng(map.get("lat")+","+map.get("lng"));
                    u.setProvince(map.get("province"));
                    unitMapper.updateByPrimaryKey(u);
                }else {
                    System.out.println(u.getName()+":is empty!!!");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test4(){
        journalMapper.updateDetailInfo();
    }

    @Test
    public void test5() throws InterruptedException {
        List<Journal> journalList = journalMapper.selectAll();
//        List<HttpRequest> requestList = new ArrayList<>();
        for (Journal journal: journalList){
            HttpGetRequest request = new HttpGetRequest(String.format("http://navi.cnki.net/knavi/JournalDetail?pcode=%s&pykm=%s", journal.getDbCode(),journal.getId()));
//            requestList.add(request);
            JournalDetailInfo.crawl(request);
            TimeUnit.SECONDS.sleep(2);
        }

    }

    @Test
    public void test6(){
        //Role role = new Role("USER");
        //User user = new User("admin","admin",new Date());
        //Role role = roleService.findByName("ROLE_USER");

        //System.out.println(role);
        unitService.updateUnitInfo();
    }

    @Test
    public void test7(){
        Jedis jedis = new Jedis("localhost",6379);
        Map<String, String> list =jedis.hgetAll("1:author:trend:count:09355430");
        System.out.println(list);
    }

    @Test
    public void test8(){
        List<Journal> journalList = journalMapper.selectAll();
        Set<String> set = new HashSet();
        for (Journal journal:journalList) {
            if (journal.getLevels() != null && !"".equals(journal.getLevels())) {
                String[] source = journal.getLevels().split(",");
                for (int i=0;i<source.length;i++){
                    set.add(source[i]);
                }
            }
        }
        System.out.println(set);
    }

    @Test
    public void test9(){
        String username = "汤庸";
        Author author = new Author();
        author.setName(username);
        List<Author> authors = authorMapper.select(author);
        if (authors.size()>0){
            Author author1 = authors.get(0);
            ResultTrend resultTrend = new ResultTrend();
            ResultDis resultDis = new ResultDis();
            resultTrend.setResultName("个人文献趋势");
            resultTrend.setrId(author1.getId());
            resultTrend.setrName(author1.getName());
            resultDis.setrId(author1.getId());
            resultDis.setrName(author1.getName());
            List<ResultTrend> resultDises1 = resultTrendMapper.select(resultTrend);
            resultDis.setResultName("个人期刊分布");
            List<ResultDis> resultDises2 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人来源分布");
            List<ResultDis> resultDises3 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人关键词分布");
            List<ResultDis> resultDises4 = resultDisMapper.select(resultDis);
            Map<String,Object> map = new HashMap<>();
            map.put("paper",resultDises1);
            map.put("journal",resultDises2);
            map.put("source",resultDises3);
            map.put("keyword",resultDises4);
//            System.out.println(JSON.toJSONString(map));


        }


    }

    @Test
    public void test10(){
        List<Paper> papers = authorMapper.getTop5Paper("05971252");
        System.out.println(papers);
    }


    @Test
    public void test11(){
        List dataList = new ArrayList();
        HashMap dataRecord1 = new HashMap();
        dataRecord1.put("id", "112000");
        dataRecord1.put("text", "廊坊银行解放道支行");
        dataRecord1.put("parentId", "110000");

        HashMap dataRecord2 = new HashMap();
        dataRecord2.put("id", "112200");
        dataRecord2.put("text", "廊坊银行三大街支行");
        dataRecord2.put("parentId", "112000");

        HashMap dataRecord3 = new HashMap();
        dataRecord3.put("id", "112100");
        dataRecord3.put("text", "廊坊银行广阳道支行");
        dataRecord3.put("parentId", "112000");

        HashMap dataRecord4 = new HashMap();
        dataRecord4.put("id", "113000");
        dataRecord4.put("text", "廊坊银行开发区支行");
        dataRecord4.put("parentId", "110000");

        HashMap dataRecord5 = new HashMap();
        dataRecord5.put("id", "100000");
        dataRecord5.put("text", "廊坊银行总行");
        dataRecord5.put("parentId", "");

        HashMap dataRecord6 = new HashMap();
        dataRecord6.put("id", "110000");
        dataRecord6.put("text", "廊坊分行");
        dataRecord6.put("parentId", "100000");

        HashMap dataRecord7 = new HashMap();
        dataRecord7.put("id", "111000");
        dataRecord7.put("text", "廊坊银行金光道支行");
        dataRecord7.put("parentId", "110000");

        dataList.add(dataRecord1);
        dataList.add(dataRecord2);
        dataList.add(dataRecord3);
        dataList.add(dataRecord4);
        dataList.add(dataRecord5);
        dataList.add(dataRecord6);
        dataList.add(dataRecord7);

        System.out.println(TreeUtils.load(dataList));
    }

}
