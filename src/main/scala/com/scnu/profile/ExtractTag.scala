package com.scnu.profile

import com.scnu.profile.scalautils.{AnalyzeHelperUnits, ConnectionPool, InitUnits, SparkUtils}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.functions._
import java.sql.Connection
import java.text.SimpleDateFormat
import java.util.Date

import com.scnu.profile.config.constants.Constants
import com.scnu.profile.utils.ServiceUtils
import org.slf4j.LoggerFactory

/**
  * Created by pcmmm on 17/7/14.
  */
object ExtractTag {
  Logger.getLogger("org").setLevel(Level.ERROR)
  val logger = LoggerFactory.getLogger(this.getClass)
  def main(args: Array[String]): Unit = {
    //初始化spark环境
    val context = InitUnits.initSparkContext()
    val sc = context._1
    val sQLContext = context._2
    SparkUtils.loadLocalTestDataToTmpTable(sc, sQLContext)
    //val serviceUtil = ServiceUtils.getServiceUtils()
//    val paperUnitRecord = paperAggrByUnit(sQLContext)
//    val groupLen = paperUnitRecord.map{ t=>
//      val key = t._1
//      val value_list = t._2
//      (key,value_list.toList.length)
//    }.sortBy(x=>x._2, false).foreach(println)
    //calAuthorPaperTrend(sQLContext)
    //calUnitDistribute(sQLContext)
    //calBySQL(sQLContext)
    //calKeyword(sQLContext)
    sc.stop()
  }

  def paperAggrByAuthor(sQLContext: SQLContext): RDD[(String, Iterable[Row])] = {
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table).map(t => (t.getString(6).split(","),t))
      .flatMap(t => {
        for(i<-0 until t._1.length-1) yield (t._1(i),t._2)
      })
    val paperGroupByAuthor = fullPaper.groupByKey()
    paperGroupByAuthor
  }

  def paperAggrByUnit(sQLContext: SQLContext):RDD[(String,Iterable[Row])] ={
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table).map(t => (t.getString(7).split(","), t))
      .flatMap(t => {
        for(i<-0 until t._1.length-1) yield (t._1(i),t._2)
      })
    val paperGroupByUnit = fullPaper.distinct().groupByKey()
    paperGroupByUnit
  }

  def paperAggrByJournal(sQLContext: SQLContext):RDD[(String,Iterable[Row])] ={
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table).map(t => (t.getString(3), t))
    val paperGroupByJournal = fullPaper.groupByKey()
    paperGroupByJournal
  }

  case class authorTrendResult(authorId:String,pubYear:Int,pubNum:Int,quoteNum:Long,downNum:Long)extends Serializable{
    override  def toString: String="%s\t%d\t%d\t%d\t%d".format(authorId, pubYear,pubNum,quoteNum,downNum)
  }

  def calAuthorPaperTrend(sQLContext: SQLContext):Unit={
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table)
      .map(t => (t.getString(7).split(","),t.getString(4).split("年")(0),t.getLong(10),t.getLong(12)))
        .flatMap(t => {
          for (i <- 0 until t._1.length -1) yield (t._1(i),t._2,t._3,t._4)
        }).map(t => ((t._1,t._2),(t._3,t._4))).distinct()
    val countTrend = fullPaper.map(t => ((t._1._1,t._1._2),1)).reduceByKey(_+_)
    //val countTrend = fullPaper.countByKey()
    val quoteTrend = fullPaper.map(t => ((t._1._1,t._1._2),t._2._1)).reduceByKey(_+_)
    val downTrend = fullPaper.map(t => ((t._1._1,t._1._2),t._2._2)).reduceByKey(_+_)
    val allTrend =quoteTrend.join(downTrend).join(countTrend)
    val allTrendMap = allTrend.map( t=> (t._1._1,t._1._2.toInt,t._2._2,t._2._1._1,t._2._1._2))

    //与author_info表关联
    val author_table = Constants.TABLE_AUTHOR_INFO
    val fullAuthor = AnalyzeHelperUnits.getFullResult(sQLContext, author_table)


    //存入mysql
    allTrend.foreachPartition(
      t => {
        if(!t.isEmpty){
          val connection = ConnectionPool.getConnection.getOrElse(null)
          t.foreach( r => {
            val sql ="insert into authorTrend(author_id,pub_year,pub_num,quote_num,down_num) values(?,?,?,?,?);"
            val data = new authorTrendResult(
              r._1._1,
              r._1._2.toInt,
              r._2._2,
              r._2._1._1,
              r._2._1._2
            )
            insertIntoMySQL(connection,sql,data)
          })
          ConnectionPool.closeConnection(connection)
        }

      }
    )

  }


  def insertIntoMySQL(con:Connection,sql:String,data:authorTrendResult): Unit ={
    // println(data.toString)
    try {
      val ps = con.prepareStatement(sql)
      ps.setString(1, data.authorId)
      ps.setInt(2, data.pubYear)
      ps.setInt(3,data.pubNum)
      ps.setLong(4, data.quoteNum)
      ps.setLong(5, data.downNum)
      ps.executeUpdate()
      ps.close()

    }catch{
      case exception:Exception=>
        logger.error("Error in execution of query "+exception.getMessage+"\n-----------------------\n"+exception.printStackTrace()+"\n-----------------------------")
    }
  }


  def calUnitDistribute(sQLContext: SQLContext):Unit = {
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table)
      .map(t => (t.getString(7).split(","),t.getString(4).split("年")(0),t.getLong(10),t.getLong(12)))
      .flatMap(t => {
        for (i <- 0 until t._1.length -1) yield (t._1(i),t._2,t._3,t._4)
      }).distinct().map(t => (t._1,(t._2,t._3,t._4)))
    val unit_table = Constants.TABLE_UNIT_INFO
    val unit_info = AnalyzeHelperUnits.getFullResult(sQLContext, unit_table)
      .map(t => (t.getString(0),(t.getString(1),t.getString(2),t.getString(3),t.getString(5),t.getString(6))))
    val paper_join_unit = fullPaper.join(unit_info)
    //paper_join_unit.foreach(println)
    val unit_count = paper_join_unit.map(t => (t._2._2._1,1)).reduceByKey(_+_)
    val province_count = paper_join_unit.map(t => (t._2._2._2,1)).reduceByKey(_+_)
    val city_count = paper_join_unit.map(t => (t._2._2._3,1)).reduceByKey(_+_)
    val unit_type_count = paper_join_unit.map( t => (t._2._2._5,1)).reduceByKey(_+_)

    //val city_count = paper_join_unit.map(t => (t._2._2._2),1)).reduceByKey(_+_).foreach(println)
  }

  def calKeywordDissturibute(sQLContext: SQLContext):Unit = {
    val table = Constants.TABLE_PAPER_INFO
    val fullPaper = AnalyzeHelperUnits.getFullResult(sQLContext, table)
      .map(t => (t.getString(6).split(","),t.getString(4).split("年")(0),t.getString(8),t.getString(2),t.getString(3)))
      .flatMap(t => {
        for (i <- 0 until t._1.length -1) yield (t._1(i),t._2,t._3,t._4,t._5)
      }).distinct().map(t => t._1)
  }

  def paperAggrByTime(rdd: RDD[(String,Iterable[Row])]):Unit={
    rdd.map( t=> {
      val authorId = t._1
      val rowInfo = t._2.toList
      for(i<-0 until rowInfo.length-1){
        val pub_year = rowInfo(i).getString(4).split("年")(0)
        val title = rowInfo(i).getString(0)


      }
    }).foreach(println)
  }

  def calBySQL(sQLContext: SQLContext):Unit = {
    val paperDF = sQLContext.sql("select * from paper_info")
    paperDF.explode("author_ids","author_id") {name :String=> name.split(",")}
  }
  def calKeyword(sQLContext: SQLContext):Unit={
    val paperDF = sQLContext.sql("select * from paper_info")
    val explodeAuAndKw = paperDF.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
      .explode("keywords","keyword"){keywords:String => keywords.split(",")}
    val aggByAuAndKwSum = explodeAuAndKw.groupBy("author_id","keyword").sum().rdd.map( t=> ((t.getString(0),t.getString(1)),t.getLong(2),t.getLong(3),t.getLong(4)))
    val aggByAuAndKwCount = explodeAuAndKw.groupBy("author_id","keyword").count().rdd.map( t=> ((t.getString(0),t.getString(1)),t.getLong(2)))


  }

  /**
    * 用户基础属性标签类
    * @param userId 用户id
    * @param calMonth 统计年份
    * @param labelId 标签id
    * @param labelValue 权重
    * @param labelCategory 标签类别：1.单位 2.专业 3.地域 4.性别 5.学历 6.职业
    * @param labelType 标签类型 1.事实标签 2.模型标签 3.预测标签
    * @param updateDate 更新日期
    */
  case class user_basic_label(userId:String, calMonth:Int, labelId:String, labelValue:Float ,
                              labelCategory:String, labelType:String, updateDate:String)


  /**
    * 用户科研行为标签类
    * @param userId 用户id
    * @param calMonth 统计月份
    * @param labelId 标签id
    * @param labelValue 权重
    * @param labelCategory 标签类别：1.研究偏好（领域价值度） 2.发表数量 3.发表频率 4.成果价值度（综合影响度，传播度）5.影响度 6.传播度
    * @param labelType 标签类型 1.事实标签 2.模型标签 3.预测标签
    * @param updateDate 更新日期
    */
  case class user_research_behavior_label(userId:String, calMonth:Int, labelId:String,
                                          labelValue:String, labelCategory:String, labelType:String,
                                          updateDate:String)


  /**
    * 用户学者网行为标签类
    * @param userId 用户id
    * @param calMonth 统计月份
    * @param labelId 标签id
    * @param labelValue 权重
    * @param labelCategory 标签类别：1.访问时间 2.访问频次 3.模块偏好 4.动态内容偏好 5.活跃度 6.忠诚度 7.价值流失预警 8.用户生命周期
    * @param labelType 标签类型 1.事实标签 2.模型标签 3.预测标签
    * @param updateDate 更新日期
    */
  case class user_scholat_behavior_label(userId:String, calMonth:Int, labelId:String,
                                         labelValue:String, labelCategory:String, labelType:String,
                                         updateDate:String)


  /**
    * 标签维表
    * @param labelId 标签id
    * @param labelName 标签名
    * @param fatherNode 标签父节点
    * @param labelCategory 标签分类
    * @param labelType 标签类型 1.事实标签 2.模型标签 3.预测标签
    * @param labelDec 标签描述
    * @param createTime 标签创建时间
    * @param updateTime 标签修改时间
    * @param createBy 创建人
    * @param modifyBy 修改人
    * @param labelState  标签状态
    */
  case class label(labelId:String, labelName:String, fatherNode:String,
                   labelCategory:String, labelType:String, labelDec:String,
                   createTime:String, labelState:String, updateTime:String,
                   createBy:String, modifyBy:String)

  def extractTrueLabel(sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
  }


  def nowDate():String = {
    val now:Date = new Date()
    val dateFormat : SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val date = dateFormat.format(now)
    date
  }
}
