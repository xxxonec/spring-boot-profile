package com.scnu.profile

import java.text.SimpleDateFormat
import java.util.Date

import com.scnu.profile.scalautils._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.sql.functions._
import org.slf4j.LoggerFactory
import redis.clients.jedis.Jedis
import scalikejdbc.{AutoSession, SQL}

import scala.collection.mutable

/**
  * Created by pcmmm on 17/8/5.
  */
object ProfileAnalyzeService {
  Class.forName("com.mysql.jdbc.Driver")
  scalikejdbc.ConnectionPool.singleton("jdbc:mysql://localhost:3306/zhiWang?characterEncoding=utf8&useSSL=true","root","asdasd")

  Logger.getLogger("org").setLevel(Level.ERROR)
  val logger = LoggerFactory.getLogger(this.getClass)
  implicit val session = AutoSession
  val redisDao = new RedisDao

  def main(args: Array[String]): Unit = {
    //初始化spark环境
    val context = InitUnits.initSparkContext()
    val sc = context._1
    val sQLContext = context._2
    SparkUtils.loadLocalTestDataToTmpTable(sc, sQLContext)
    val taskId= 1
    //SparkUtils.loadLocalTestDataToHiveContextTable(sc, hiveContext)
//    macroProfile(taskId,sQLContext)
//    hIndex(taskId,sQLContext)
//    testRedis()

//    authorProfile(taskId,sQLContext)
    keyword(taskId,sQLContext)
    //authorProfileWithHive(hiveContext)
    //unitProfile(sQLContext)
//    val taskId = 1
//    try {
//      taskId match {
//        case 1 => {
//          macroProfile(sQLContext)
//          println("分析完成")
//        }
//        case 2 => {
//
//        }
//        case 3 => {
//
//        }
//        case 4 => {
//
//        }
//      }
//    }catch {
//      case e: Exception => println("没有匹配的需求编号")
//    }
    sc.stop()
  }
  /**
    * 分布模型
    * @param taskId
    * @param rName
    * @param count
    * @param resultName 
    */
  case class resultDis(taskId:Int,rId:String,rName:String,count:Int,resultName:String)


  /**
    * 趋势模型
    * @param taskId
    * @param rId
    * @param rName
    * @param rYear
    * @param paperCount
    * @param quoteNum
    * @param quote2Num
    * @param downloadNum
    * @param resultName 
    */
  case class resultTrend(taskId:Int,rId:String,rName:String,resultName:String,rYear:Int,paperCount:Long,quoteNum:Long,quote2Num:Long,downloadNum:Long)

  /**
    * top模型
    * @param taskId
    * @param rId
    * @param rName
    * @param resultName 
    * @param paperCount
    * @param quoteNum
    * @param quote2Num
    * @param downloadNum
    */
  case class resultTop(taskId:Int,resultName:String,rId:String, rName:String, paperCount:Long, quoteNum:Long, quote2Num:Long,downloadNum:Long)


  /**paper_info表
    * id|title|summary|journal_id|pub_date|categorys|author_ids|org_ids|keywords|funds|quote_num|quote2_num|download_num
    *Author_info表
    * id|name|unit_id|domains
    *Unit_info表
    * id|name|city|city|district|lat_lng|category|rank|
    *Journal_info表
    * id|name|papers_num|quote_num|pub_cycle
    *
    * @param sQLContext
    */
  def macroProfile(taskId:Int, sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    allPaper.createOrReplaceTempView("allPaper")
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")

    //总体地域分布
    val addressDis = sQLContext.sql("select case when un.city is null or un.city=='' then un.province else split(un.city,'市')[0] end as city,count(1) count from author_info au,unit_info un where " +
      "au.unit_id = un.id and un.city is not null and un.city !='' group by un.city,un.province order by count desc ")
    addressDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name, r_name, count)
                values (?, ?, ?, ?)
              """)
            .bind(taskId, "总体地域分布", r.getString(0), r.getLong(1)).update.apply()
        })
      }
    })

    //总体学校分布
    val schoolDis = sQLContext.sql("select ai.unit_id,ui.name,count(ai.unit_id) count from author_info ai,unit_info ui where ai.unit_id = ui.id group by ai.unit_id,ui.name order by count desc limit 50")
    schoolDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name,r_id, r_name, count)
                values (?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体学校分布", r.getString(0), r.getString(1),r.getLong(2)).update.apply()
        })
      }
    })

    //总体学科分布
    val explodeDomains = sQLContext.sql("select * from author_info").explode("domains","domain"){domains:String => domains.split(",")}
    explodeDomains.createOrReplaceTempView("explodeDomains")
    val subjectDis = sQLContext.sql("select ed.domain,count(1) count from explodeDomains ed where ed.domain is not null and ed.domain!='' group by ed.domain " +
      "order by count desc limit 50")
    subjectDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name, r_name, count)
                values (?, ?, ?, ?)
              """)
            .bind(taskId, "总体学科分布", r.getString(0), r.getLong(1)).update.apply()
        })
      }
    })

    //学术成果研究机构（文献量）排名（总体机构文献排名）
    //TODO 如果作者都是一个单位的话算一篇
    val unitPaperNumTop10 = sQLContext.sql("select un.id,un.name,count(1) count ,sum(ep.quote_num),sum(ep.quote2_num),sum(ep.download_num) " +
      "from explodePaper ep,author_info au,unit_info un " +
      "where ep.author_id=au.id and au.unit_id = un.id " +
      "group by un.id,un.name order by count desc limit 20")

    unitPaperNumTop10.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, paper_count, quote_num, quote2_num, download_num)
                values (?, ?, ?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体机构文献排名", r.getString(0), r.getString(1), r.getLong(2), r.getLong(3), r.getLong(4), r.getLong(5)).update.apply()
        })
      }
    })
    //学术成果期刊（文献量）排名（总体期刊文献排名）
    val journalPaperNumTop = sQLContext.sql("select j.id,j.name,count(1) count ,sum(p.quote_num),sum(p.quote2_num),sum(p.download_num) " +
      "from paper_info p,journal_info j " +
      "where p.journal_id = j.id " +
      "group by j.id,j.name order by count desc limit 20")

    journalPaperNumTop.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, paper_count, quote_num, quote2_num, download_num)
                values (?, ?, ?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体期刊文献排名", r.getString(0), r.getString(1), r.getLong(2), r.getLong(3), r.getLong(4), r.getLong(5)).update.apply()
        })
      }
    })
    //学术成果高关注度机构排名（被引量、下载量）（总体机构关注排名）
    val explodeOrg = sQLContext.sql("select * from paper_info").explode("org_ids","org_id"){org_ids:String => org_ids.split(",")}
    explodeOrg.createOrReplaceTempView("explodeOrg")
    val unitHighAttenTop = sQLContext.sql("select un.id,un.name,sum(eo.quote_num) sum_quo,sum(eo.download_num) sum_down" +
      " from explodeOrg eo, unit_info un" +
      " where eo.org_id = un.id" +
      " group by un.id,un.name order by sum_quo desc limit 20")
    unitHighAttenTop.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, quote_num, download_num)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体机构关注排名", r.getString(0), r.getString(1), r.getLong(2), r.getLong(3)).update.apply()
        })
      }
    })
    //学术成果高关注度期刊排名（被引量、下载量)（总体期刊关注排名）
    val journalHighAttenTop = sQLContext.sql("select j.id,j.name,sum(p.quote_num) sum_quo,sum(p.download_num) sum_down" +
      " from paper_info p, journal_info j" +
      " where p.journal_id = j.id" +
      " group by j.id,j.name order by sum_quo desc limit 20")
    journalHighAttenTop.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, quote_num, download_num)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体期刊关注排名", r.getString(0), r.getString(1), r.getLong(2), r.getLong(3)).update.apply()
        })
      }
    })

    //学术成果研究方向分布（总体研究方向分布）
    val explodeKey = sQLContext.sql("select * from paper_info").explode("keywords","keyword"){keywords:String => keywords.split(",")}
    explodeKey.createOrReplaceTempView("explodeKey")
//    val keywordDis = explodeKey.select(col("keyword").isNotNull.and(col("keyword").notEqual(""))).groupBy("keyword").count().alias("count").orderBy(desc("count")).limit(50)
    val keywordDis = sQLContext.sql("select keyword,count(1) count from explodeKey where keyword is not null and keyword !='' group by keyword order by count desc limit 200")
    keywordDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name, r_name, count)
                values (?, ?, ?, ?)
              """)
            .bind(taskId, "总体研究方向分布", r.getString(0), r.getLong(1)).update.apply()
        })
      }
    })
    //按年份聚合(总体文献趋势)
    val yearAggr = sQLContext.sql("select ep.pub_date,count(1) count, sum(quote_num) sum_quo,sum(quote2_num),sum(download_num) sum_down" +
      " from allPaper ep where ep.pub_date is not null and ep.pub_date !='' and ep.pub_date!='null'  group by ep.pub_date order by ep.pub_date ")
    yearAggr.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_trend (task_id, result_name, r_year, paper_count, quote_num,quote2_num, download_num)
                values (?, ?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体文献趋势", r.getString(0), r.getLong(1), r.getLong(2), r.getLong(3), r.getLong(4)).update.apply()
        })
      }
    })


    //文献数前十的author(总体学者文献排名)
    val paperCount = sQLContext.sql("select a.author_id,b.name,count(a.author_id) count,sum(a.quote_num),sum(a.quote2_num), sum(download_num) from explodePaper a, author_info b where a.author_id=b.id group by a.author_id,b.name order by count desc limit 20")

    paperCount.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, paper_count,quote_num,quote2_num,download_num)
                values (?, ?, ?, ?, ?, ?, ?, ?)
                            """)
            .bind(taskId, "总体学者文献排名", r.getString(0), r.getString(1), r.getLong(2),r.getLong(3),r.getLong(4),r.getLong(5)).update.apply()
        })
      }
    })

    //总体学者关注排名
    val authorImpact = sQLContext.sql("select a.author_id,b.name,sum(a.quote_num) quote_num, sum(download_num) from explodePaper a, author_info b where a.author_id=b.id group by a.author_id,b.name order by quote_num desc limit 20")

    authorImpact.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name,quote_num,download_num)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体学者关注排名", r.getString(0), r.getString(1), r.getLong(2), r.getLong(3)).update.apply()
        })
      }
    })


    //总体文献关注排名
    val paperImpactTop = sQLContext.sql("select ap.id,ap.title,ap.quote_num from allPaper ap order by quote_num desc limit 20")

    paperImpactTop.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, quote_num)
                values (?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体文献关注排名", r.getString(0), r.getString(1), r.getLong(2)).update.apply()
        })
      }
    })

    //总体文献下载排名
    val paperDownloadTop = sQLContext.sql("select ap.id,ap.title,ap.download_num from allPaper ap order by download_num desc limit 20")

    paperDownloadTop.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_top (task_id, result_name, r_id, r_name, download_num)
                values (?, ?, ?, ?, ?)
              """)
            .bind(taskId, "总体文献下载排名", r.getString(0), r.getString(1), r.getLong(2)).update.apply()
        })
      }
    })

  }

  def hIndex(taskId:Int, sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    allPaper.createOrReplaceTempView("allPaper")
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")

    // h-index 按引用数量来desc 排序 看当前序号<当前引用量
    // i10-index 发表文章数被引用10次以上的个数
    val h_index = sQLContext.sql("select d.id,d.name,max(d.index) index from " +
      "(select c.id,c.name, case when c.max_num>=1 and c.flag=1 then c.max_num else 0 end as index from " +
      "(select b.id,b.name,max(b.num_desc) as max_num,b.flag from " +
      "(select a.id,a.name,a.quote_num,a.num_desc,case when a.quote_num >= a.num_desc then 1 ELSE 0 END AS flag from " +
      "(select ai.id,ai.name,ep.quote_num, row_number() over (partition by ai.id,ai.name order by quote_num desc) num_desc " +
      "from explodePaper ep,author_info ai where ep.author_id=ai.id) a) b group by b.id,b.name,b.flag) c) d group by d.id,d.name")
    h_index.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into h_index (task_id, r_id, r_name, h_index)
                values (?, ?, ?, ?)
              """)
            .bind(taskId, r.getString(0), r.getString(1), r.getInt(2)).update.apply()
        })
      }
    })


  }

  def testRedis():Unit={
//    val jr:Jedis = null;
//    try{
//      val jr = new Jedis("localhost",6379)
////      val map = new mutable.HashMap[String,String]()
////      var map:mutable.Map[String,String] = Map("c0"->"1")
//      val map = Map("c1"->"111","c1"->"111","c3"->"333")
//      jr.hmset("asd",map)
//    }

    val map:mutable.Map[String,String] = mutable.Map("c1"->"111","c2"->"222","c3"->"333")
    redisDao.hmset("a",map)
  }

  def source(taskId:Int, sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    allPaper.createOrReplaceTempView("allPaper")
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")
    val explodeJournal = sQLContext.sql("select * from journal_info").explode("levels","level"){levels:String => levels.split(",")}
    explodeJournal.createOrReplaceTempView("explodeJournal")
  }

  def keyword(taskId:Int,sQLContext: SQLContext):Unit= {
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")
    val explodeJournal = sQLContext.sql("select * from journal_info").explode("levels","level"){levels:String => levels.split(",")}
    explodeJournal.createOrReplaceTempView("explodeJournal")
    val explodeKey = sQLContext.sql("select * from explodePaper").explode("keywords","keyword"){keywords:String => keywords.split(",")}
    explodeKey.createOrReplaceTempView("explodeKey")
    val keywordDis = sQLContext.sql(
      " select b.id,b.name,b.keyword,b.count,b.rank from " +
      " (select a.id,a.name,a.keyword,a.count,row_number() over(partition by a.id,a.name order by a.count desc) rank from " +
      " (select au.id,au.name,ek.keyword,count(1) count" +
      " from explodeKey ek, author_info au" +
      " where ek.author_id= au.id and ek.keyword is not null and ek.keyword!='' group by au.id,au.name,ek.keyword) a) b" +
      " where b.rank <= 50 ")

    keywordDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name,r_id, r_name,j_name, count)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "个人关键词分布", r.getString(0), r.getString(1),r.getString(2), r.getLong(3)).update.apply()
        })
      }
    })
  }

  def authorProfile(taskId:Int,sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    val explodePaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    val explodeJournal = sQLContext.sql("select * from journal_info").explode("levels","level"){levels:String => levels.split(",")}
    explodeJournal.createOrReplaceTempView("explodeJournal")
    val flatPaper = allPaper.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")
    //学术成果量走势
    val paperNumTrend = sQLContext.sql("select ep.author_id,ai.name, ep.pub_date, count(1) count, sum(quote_num) sum_quo,sum(download_num) sum_down" +
      " from explodePaper ep,author_info ai" +
      " where ep.author_id=ai.id and ep.pub_date is not null and ep.pub_date !='' and ep.pub_date!='null'" +
      " group by ep.author_id,ai.name, ep.pub_date ")
//    paperNumTrend.foreachPartition( t=> {
//      if (!t.isEmpty){
//        t.foreach( r=> {
//          val map:mutable.Map[String,String] = mutable.Map("id"->r.getString(0),
//                                                          "name"->r.getString(1),
//                                                          "year"->r.getString(2),
//                                                          "count"->r.getLong(3).toString,
//                                                          "quote"->r.getLong(4).toString,
//                                                          "download"->r.getLong(5).toString)
//          val key = taskId+":author:trend:count:"+r.getString(0)
//          redisDao.hmset(key,map)
//          redisDao.rpush(taskId+":author:trend:count:list",String.valueOf(r.getString(0)))
//        })
//      }
//    })
    paperNumTrend.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_trend (task_id, result_name, r_id,r_name,r_year, paper_count, quote_num, download_num)
                values (?, ?, ?, ?, ?, ?, ?,?)
              """)
            .bind(taskId, "个人文献趋势", r.getString(0), r.getString(1), r.getString(2), r.getLong(3), r.getLong(4), r.getLong(5)).update.apply()
        })
      }
    })



//    val paperCountTrend = sQLContext.sql("select ep.author_id,ai.name,ej.level,ep.pub_date,count(1) count " +
//      "from explodePaper ep , explodeJournal ej ,author_info ai where ai.id=ep.author_id and ep.journal_id=ej.id " +
//      "group by ep.author_id,ai.name,ej.level ,ep.pub_date ")
//

    val journalDis = sQLContext.sql("select ep.author_id,ai.name,ep.journal_id,ji.name,count(1)" +
      " from explodePaper ep ,journal_info ji ,author_info ai" +
      " where ep.journal_id =ji.id and ep.author_id=ai.id group by ep.author_id,ai.name,ep.journal_id,ji.name")
    journalDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name,r_id, r_name,j_id,j_name, count)
                values (?, ?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "个人期刊分布", r.getString(0), r.getString(1),r.getString(2),r.getString(3), r.getLong(4)).update.apply()
        })
      }
    })
//
//
//
    val sourceDis = sQLContext.sql("select ep.author_id,ai.name,ej.level,count(1) count" +
      " from explodePaper ep ,explodeJournal ej,author_info ai" +
      " where ep.journal_id=ej.id and ep.author_id=ai.id  group by ep.author_id,ai.name,ej.level")
    sourceDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name,r_id, r_name,j_name, count)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "个人来源分布", r.getString(0), r.getString(1),r.getString(2), r.getLong(3)).update.apply()
        })
      }
    })

    //学术成果类型分布
    //学术成果传播度（被引量）走势 上面有了
    //学术成果用户关注度走势
    //学术研究方向走向
    val explodeKey = sQLContext.sql("select * from explodePaper").explode("keywords","keyword"){keywords:String => keywords.split(",")}
    explodeKey.createOrReplaceTempView("explodeKey")
//    val keywordTrendByYear = sQLContext.sql("select au.id,au.name,ek.keyword,ek.pub_date,count(1) count,sum(ek.quote_num) sum_quo,sum(ek.download_num) sum_down" +
//      " from explodeKey ek, author_info au" +
//      " where ek.author_id= au.id and ek.keyword is not null and ek.keyword!='' group by au.id,au.name,ek.pub_date,ek.keyword")
//    keywordTrendByYear.foreachPartition( t=> {
//      if (!t.isEmpty){
//        t.foreach( r=> {
//          SQL("""
//                insert into result_trend (task_id, result_name, r_id,r_name,r_year,j_name, paper_count, quote_num, download_num)
//                values (?, ?, ?, ?, ?, ?, ?,?)
//              """)
//            .bind(taskId, "个人关键词趋势", r.getString(0), r.getString(1), r.getString(2), r.getLong(3), r.getLong(4), r.getLong(5)).update.apply()
//        })
//      }
//    })

    val keywordDis = sQLContext.sql("select b.id,b.name,b.keyword,b.count from " +
      " (select a.id,a.name,a.keyword,a.count,row_number() over(partition by a.id,a.name order by a.count desc) rank from " +
      " (select au.id,au.name,ek.keyword,count(1) count" +
      " from explodeKey ek, author_info au" +
      " where ek.author_id= au.id and ek.keyword is not null and ek.keyword!='' group by au.id,au.name,ek.keyword) a) b" +
      " where b.rank <=50")
    keywordDis.foreachPartition( t=> {
      if (!t.isEmpty){
        t.foreach( r=> {
          SQL("""
                insert into result_dis (task_id, result_name,r_id, r_name,j_name, count)
                values (?, ?, ?, ?, ?, ?)
              """)
            .bind(taskId, "个人关键词分布", r.getString(0), r.getString(1),r.getString(2), r.getLong(3)).update.apply()
        })
      }
    })

//    val w = Window.partitionBy("author_id").orderBy(col("quote_num")).rowsBetween(1, 10)
//    val paperCountTop10 = sQLContext.sql("select * from explodePaper").toDF()
//      .select(col("author_id"),col("title"),row_number().over(w)).show()

//    val paperCountTop10 = sQLContext
//      .sql("select author_id,title,quote_num,download_num,row_number() over (partition by author_id order by quote_num,download_num desc) num from explodePaper")

    //学术研究方向影响力分布
    //学术合作关系图谱
  }



  def unitProfile(sQLContext: SQLContext):Unit={
    val getPubYear:(String => String) = (args:String) => {args.split("年")(0)}
    val getPubYearFunc = udf(getPubYear)
    val allPaper = sQLContext.sql("select * from paper_info").withColumn("pub_date", getPubYearFunc(col("pub_date")))
    val explodeOrg = allPaper.explode("org_ids","org_id"){org_ids:String => org_ids.split(",")}.distinct()
    explodeOrg.createOrReplaceTempView("explodeOrg")
    val explodePaper = explodeOrg.explode("author_ids","author_id"){author_ids:String => author_ids.split(",")}
    explodePaper.createOrReplaceTempView("explodePaper")
    val explodeKey = explodeOrg.explode("keywords","keyword"){keywords:String => keywords.split(",")}
    explodeKey.createOrReplaceTempView("explodeKey")
    //    学术成果量走势
    val unitNumTrend = sQLContext.sql("select un.id,un.name, ap.pub_date, count(1) count, sum(ap.quote_num) sum_quo,sum(ap.download_num) sum_down" +
      " from explodeOrg ap,unit_info un where ap.org_id = un.id group by un.id,un.name,ap.pub_date").orderBy(desc("count"),desc("sum_quo"),desc("sum_down"))
    //    学科分布
    val explodeDomains = sQLContext.sql("select * from author_info").explode("domains","domain"){domains:String => domains.split(",")}
    explodeDomains.createOrReplaceTempView("explodeDomains")
    val subjectDis = sQLContext.sql("select distinct(ep.id) p_id,un.id un_id,un.name,au.domain domain" +
      " from explodePaper ep, explodeDomains au, unit_info un" +
      " where ep.org_id=un.id and ep.author_id=au.id" +
      " ").groupBy("un_id","name","domain").count().select("un_id","name","domain","count").orderBy(desc("count"))
    //    研究方向分布
    val keywordDis = sQLContext.sql("select un.id,un.name,ek.keyword,count(1) count" +
      " from explodeKey ek, unit_info un" +
      " where ek.org_id=un.id and ek.keyword != \"\"" +
      " group by un.id,un.name,ek.keyword order by count desc limit 100")
    //    该机构学者发文量排行
//    val unitTop10Author = sQLContext.sql("SELECT un_id,un_name,au_id,au_name,count,sum_quo,sum_down " +
//      "FROM " +
//      "(SELECT un.id un_id,un.name un_name,au.id au_id,au.name au_name,row_number() " +
//      "OVER" +
//      "(PARTITION BY un_id,un_name,au_id,au_name ORDER BY count(1) count,sum(ep.quote_num) sum_quo, sum(ep.download_num) sum_down DESC) rank " +
//      "FROM explodePaper ep,unit_info un,author_info au " +
//      "WHERE ep.org_id = un.id and ep.author_id=au.id) a " +
//      "WHERE a.rank<=10").show(1000)
    //    受关注文献排行（被引量、下载量）
    //    该机构学术传播度（被引量）
    //    该机构学术用户关注度（下载量趋势）

    //val unitTop10Author1 = sQLContext.sql("SELECT id ,row_number() OVER(partition by id ORDER BY id  DESC) rank FROM unit_info")

//    val w = Window.partitionBy("country").orderBy(count(col("id"))).rowsBetween(1, 10)
//    val result = sQLContext.sql("select * from paper_info").toDF()
//      .select(col("s"),row_number().over(w)).show()
  }




  def nowDate():String = {
    val now:Date = new Date()
    val dateFormat : SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val date = dateFormat.format(now)
    date
  }




}
