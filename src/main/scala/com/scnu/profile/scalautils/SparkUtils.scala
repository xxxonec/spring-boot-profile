package com.scnu.profile.scalautils

import com.scnu.profile.config.ConfigurationManager
import com.scnu.profile.config.constants.Constants
import org.apache.spark.SparkContext
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.types.{StructType, _}


object SparkUtils {

    /**
      * 加载本地测试数据到注册表
      *
      * @param sc
      * @param sqlContext
      */
    def loadLocalTestDataToTmpTable(sc: SparkContext, sqlContext: SQLContext): Unit = {

        val paperSchema = StructType(
            List(
                StructField("id",StringType,true),
                StructField("title",StringType,true),
                StructField("summary",StringType,true),
                StructField("journal_id",StringType,true),
                StructField("pub_date",StringType,true),
                StructField("categorys",StringType,true),
                StructField("author_ids",StringType,true),
                StructField("org_ids",StringType,true),
                StructField("keywords",StringType,true),
                StructField("funds",StringType,true),
                StructField("quote_num",LongType,true),
                StructField("quote2_num",LongType,true),
                StructField("download_num",LongType,true)
            )
        )
        val paper_path = ConfigurationManager.getProperty(Constants.LOCAL_PAPER_DATA_PATH)
        //从指定位置创建RDD
        val paperRDD = sc.textFile(paper_path).map(_.split("\\|",13))
        val paperRowRDD = paperRDD.map(s => Row(
            s(0).trim,
            s(1).trim,
            s(2).trim,
            s(3).trim,
            s(4).trim,
            s(5).trim,
            s(6).trim,
            s(7).trim,
            s(8).trim,
            s(9).trim,
            s(10).trim.toLong,
            s(11).trim.toLong,
            s(12).trim.toLong

        ))
        //将schema信息应用到rowRDD上
        val paperDataFrame = sqlContext.createDataFrame(paperRowRDD, paperSchema)
        //注册临时sessionAction表
        paperDataFrame.createOrReplaceTempView(Constants.TABLE_PAPER_INFO)

        val authorSchema = StructType(
            List(
                StructField("id",StringType,true),
                StructField("name",StringType,true),
                StructField("unit_id",StringType,true),
                StructField("domains",StringType,true)
            )
        )
        val author_path = ConfigurationManager.getProperty(Constants.LOCAL_AUTHOR_DATA_PATH)
        val authorRDD = sc.textFile(author_path).map(_.split("\\|",4))
        //将RDD映射成rowRDD
        val authorRowRDD = authorRDD.map(s => Row(
            s(0).trim,
            s(1).trim,
            s(2).trim,
            s(3).trim
        ))
        val authorDataFrame = sqlContext.createDataFrame(authorRowRDD, authorSchema)
        authorDataFrame.createOrReplaceTempView(Constants.TABLE_AUTHOR_INFO)


        val unitSchema = StructType(
            List(
                StructField("id", StringType, true),
                StructField("name", StringType, true),
                StructField("province", StringType, true),
                StructField("city", StringType, true),
                StructField("district", StringType, true),
                StructField("lat_lng", StringType, true),
                StructField("category", StringType, true),
                StructField("rank", StringType, true)
            )
        )
        val unit_path = ConfigurationManager.getProperty(Constants.LOCAL_UNIT_DATA_PATH)
        val unitRDD = sc.textFile(unit_path).map(_.split("\\|",8))
        val unitRowRDD = unitRDD.map(s => Row(
            s(0).trim,
            s(1).trim,
            s(2).trim,
            s(3).trim,
            s(4).trim,
            s(5).trim,
            s(6).trim,
            s(7).trim
        ))
        val unitDataFrame = sqlContext.createDataFrame(unitRowRDD, unitSchema)
        unitDataFrame.createOrReplaceTempView(Constants.TABLE_UNIT_INFO)



        val journalSchema = StructType(
            List(
                StructField("id", StringType, true),
                StructField("name", StringType, true),
                StructField("levels", StringType, true),
                StructField("papers_num", StringType, true),
                StructField("quote_num", StringType, true),
                StructField("pub_cycle", StringType, true)
            )
        )
        val journal_path = ConfigurationManager.getProperty(Constants.LOCAL_JOURNAL_DATA_PATH)
        val journalRDD = sc.textFile(journal_path).map(_.split("\\|",6))
        val journalRowRDD = journalRDD.map(s => Row(
            s(0).trim,
            s(1).trim,
            s(2).trim,
            s(3).trim,
            s(4).trim,
            s(5).trim
        ))
        //
        val journalDataFrame = sqlContext.createDataFrame(journalRowRDD, journalSchema)
//        journalDataFrame.registerTempTable(Constants.TABLE_JOURNAL_INFO)
        journalDataFrame.createOrReplaceTempView(Constants.TABLE_JOURNAL_INFO)
    }
}