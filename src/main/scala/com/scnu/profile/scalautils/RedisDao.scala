package com.scnu.profile.scalautils
import java.util

import com.scnu.profile.scalautils.RedisConnector.clients
import redis.clients.jedis.{HostAndPort, Jedis, JedisCluster}

import scala.collection.JavaConversions._
import scala.collection.mutable
/**
  * Created by pcmmm on 17/9/13.
  */
class RedisDao extends Serializable{

  /**
    *
    * @param key
    * @param value
    * @return
    */

  def set(key:String,value:Any): Unit ={

    clients.set(key,String.valueOf(value))

  }

  /**
    *
    * @param key
    * @param time
    * @return
    */

  def hget(key:String,time:Long): Option[String] ={

    val value=clients.hget(key,String.valueOf(time))
    if(value==null)
      None
    else
      Some(value)

  }

  /**
    *
    * @param key
    * @param time
    * @param value
    * @return
    */
  def hset(key:String,time:Long,value:Any): Boolean ={
    clients.hset(key,String.valueOf(time),String.valueOf(value))==1

  }

  /**
    *
    * @param key
    * @param map
    */
  def hmset(key:String,map:mutable.Map[String,String]): Unit ={
    val map2=mutable.Map[String,String]()
    map.foreach{case (key:String,value:String)=>
      map2.put(key,value)
    }

    clients.hmset(key,mapAsJavaMap(map2))

  }

  /**
    *
    * @param key
    * @param time
    * @return
    */
  def hdel(key:String,time:Any): Option[Long] ={

    Some(clients.hdel(key,String.valueOf(time)))

  }

  /**
    *
    * @param key
    * @param times
    * @return
    */
  def rpush(key:String,times:Any): Option[Long] ={

    Some(clients.rpush(key,String.valueOf(times)))

  }


  /**
    *
    * @param key
    * @return
    */
  def lpop(key:String): Option[Long] ={

    val time=clients.lpop(key)
    if(time==null)
      None
    else
      Some(time.toLong)
  }


  /**
    *
    * @param key
    * @return
    */
  def lhead(key:String): Option[Long] ={

    val head=clients.lindex(key,0)
    if(head==null)
      None
    else
      Some(head.toLong)
  }

}

object RedisConnector {
//  private val clients = new Jedis("localhost",6379)
//  private val jedisClusterNodes = new util.HashSet[HostAndPort]()
//  jedisClusterNodes.add(new HostAndPort("localhost",6379))
//  jedisClusterNodes.add(new HostAndPort("192.168.6.53",6380))
//  jedisClusterNodes.add(new HostAndPort("192.168.6.53",6379))

//  val clients = new JedisCluster(jedisClusterNodes)
  val clients = new Jedis("localhost",6379)
}