CREATE TABLE label(
  label_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  label_name VARCHAR (255),
  father_node INTEGER ,
  label_category INTEGER ,
  label_type INTEGER ,
  label_dec TEXT,
  create_time DATETIME,
  label_state INTEGER ,
  update_time DATETIME,
  create_by VARCHAR (255),
  modify_by VARCHAR (255)
);

CREATE TABLE user_scholat_behavior_label(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id VARCHAR (255),
  cal_month INTEGER ,
  label_id INTEGER ,
  label_value SMALLINT ,
  label_category INTEGER ,
  label_type INTEGER ,
  update_date DATETIME
);

CREATE TABLE user_research_behavior_label(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id VARCHAR (255),
  cal_month INTEGER ,
  label_id INTEGER ,
  label_value SMALLINT ,
  label_category INTEGER ,
  label_type INTEGER ,
  update_date DATETIME
);

CREATE TABLE user_basic_label(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id VARCHAR (255),
  cal_month INTEGER ,
  label_id INTEGER ,
  label_value SMALLINT ,
  label_category INTEGER ,
  label_type INTEGER ,
  update_date DATETIME
);

CREATE TABLE result_trend(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  task_id VARCHAR (255),
  result_name VARCHAR(255),
  r_id VARCHAR(255),
  r_name VARCHAR(255),
  r_year VARCHAR(255),
  paper_count BIGINT,
  quote_num BIGINT,
  quote2_num BIGINT,
  download_num BIGINT
);



CREATE TABLE result_dis(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  task_id VARCHAR (255),
  result_name VARCHAR(255),
  r_id VARCHAR(255),
  r_name VARCHAR(255),
  count BIGINT,

);

CREATE TABLE result_top(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  task_id VARCHAR (255),
  result_name VARCHAR(255),
  r_id VARCHAR(255),
  r_name VARCHAR(255),
  paper_count BIGINT,
  quote_num BIGINT,
  quote2_num BIGINT,
  download_num BIGINT
);

DROP TABLE IF EXISTS h_index;
CREATE TABLE h_index(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  task_id VARCHAR(255),
  r_id VARCHAR (255),
  r_name VARCHAR(255),
  h_index INT
);