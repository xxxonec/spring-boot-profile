package com.scnu.profile.module.system.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.system.service.TaskService;
import com.scnu.profile.mybatis.mapper.TaskMapper;
import com.scnu.profile.mybatis.model.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/09/03.
 */
@Service
@Transactional
public class TaskServiceImpl extends AbstractService<Task> implements TaskService {
    @Resource
    private TaskMapper taskMapper;

}
