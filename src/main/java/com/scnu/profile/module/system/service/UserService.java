package com.scnu.profile.module.system.service;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.core.service.Service;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
public interface UserService extends Service<User> {
    User findByUserName(String username);
    Long saveAndReturnId(User user);
}
