package com.scnu.profile.module.system.service.impl;

import com.scnu.profile.mybatis.mapper.RoleMapper;
import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.module.system.service.RoleService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
@Service
@Transactional
public class RoleServiceImpl extends AbstractService<Role> implements RoleService {
    @Resource
    private RoleMapper roleMapper;

    @Override
    public Role findByName(String name) {
        Role role = new Role();
        role.setRoleName(name);
        if(roleMapper.select(role).size()>0){
            return roleMapper.select(role).get(0);
        }else {
            return null;
        }
    }

    @Override
    public Long saveAndReturnId(Role role) {
        roleMapper.insertAndGetId(role);
        return role.getId();
    }
}
