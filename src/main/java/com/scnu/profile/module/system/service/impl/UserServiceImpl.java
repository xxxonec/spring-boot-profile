package com.scnu.profile.module.system.service.impl;

import com.scnu.profile.mybatis.mapper.UserMapper;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.module.system.service.UserService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public User findByUserName(String username) {
        User user = new User();
        user.setUsername(username);
        if(userMapper.select(user).size()>0){
            return userMapper.select(user).get(0);
        }else {
            return null;
        }
    }

    @Override
    public Long saveAndReturnId(User user) {
        userMapper.insertAndGetId(user);
        Long id = user.getId();
        return id;
    }
}
