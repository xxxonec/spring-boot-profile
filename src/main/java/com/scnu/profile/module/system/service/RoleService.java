package com.scnu.profile.module.system.service;
import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.core.service.Service;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
public interface RoleService extends Service<Role> {
    Role findByName(String name);
    Long saveAndReturnId(Role role);
}
