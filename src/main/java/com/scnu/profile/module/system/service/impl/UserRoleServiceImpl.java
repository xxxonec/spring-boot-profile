package com.scnu.profile.module.system.service.impl;

import com.scnu.profile.mybatis.mapper.UserRoleMapper;
import com.scnu.profile.mybatis.model.UserRole;
import com.scnu.profile.module.system.service.UserRoleService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
@Service
@Transactional
public class UserRoleServiceImpl extends AbstractService<UserRole> implements UserRoleService {
    @Resource
    private UserRoleMapper userRoleMapper;

}
