package com.scnu.profile.module.system.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.system.service.LabelService;
import com.scnu.profile.mybatis.mapper.LabelMapper;
import com.scnu.profile.mybatis.model.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LabelServiceImpl extends AbstractService<Label> implements LabelService{
    @Autowired
    private LabelMapper labelMapper;

    @Override
    public Integer insertAndGetId(Label label) {
        return labelMapper.insertAndGetId(label);
    }

    @Override
    public List<Label> getChildLabel(Integer labelId) {
        return labelMapper.getChildLabel(labelId);
    }

    @Override
    public Label getFatherLabel(Integer labelId) {
        return labelMapper.getFatherLabel(labelId);
    }

    @Override
    public Integer getFatherLabelCount() {
        return labelMapper.getFatherLabelCount();
    }

    @Override
    public Integer getChrildLabelCount(Integer labelId) {
        return labelMapper.getChrildLabelCount(labelId);
    }
}
