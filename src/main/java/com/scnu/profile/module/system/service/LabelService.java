package com.scnu.profile.module.system.service;

import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.Label;

import java.util.List;

public interface LabelService extends Service<Label> {

    /**
     * 插入并获取标签id
     * @param label
     * @return
     */
    Integer insertAndGetId(Label label);

    /**
     * 获取当前节点子标签
     * @param labelId
     * @return
     */
    List<Label> getChildLabel (Integer labelId);


    /**
     * 获取当前节点父标签
     * @param labelId
     * @return
     */
    Label getFatherLabel(Integer labelId);

    /**
     * 获取父标签数目
     * @return
     */
    Integer getFatherLabelCount();

    /**
     * 获取子标签数目
     * @param labelId
     * @return
     */
    Integer getChrildLabelCount(Integer labelId);
}
