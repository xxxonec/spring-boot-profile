package com.scnu.profile.module.system.service;
import com.scnu.profile.mybatis.model.UserRole;
import com.scnu.profile.core.service.Service;


/**
 * Created by CodeGenerator on 2017/08/15.
 */
public interface UserRoleService extends Service<UserRole> {

}
