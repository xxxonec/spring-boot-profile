package com.scnu.profile.module.system.service;


import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.Task;

/**
 * Created by CodeGenerator on 2017/09/03.
 */
public interface TaskService extends Service<Task> {

}
