package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.mybatis.mapper.LabelMapper;
import com.scnu.profile.mybatis.model.Label;
import com.scnu.profile.module.scholat.service.LabelService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
//
//
///**
// * Created by CodeGenerator on 2017/07/27.
// */
//@Service
//@Transactional
//public class LabelServiceImpl extends AbstractService<Label> implements LabelService {
//    @Resource
//    private LabelMapper labelMapper;
//
//    @Override
//    public Boolean checkLabel(String labelName) {
//        Label label = new Label();
//        label.setLabelName(labelName);
//        int count = labelMapper.selectCount(label);
//        if (count > 0){
//            return true;
//        }else {
//            return false;
//        }
//    }
//
//    @Override
//    public Integer getIdFromName(String labelName) {
//        Label label = new Label();
//        label.setLabelName(labelName);
//        List<Label> labelList = labelMapper.select(label);
//        return labelList.get(0).getLabelId();
//    }
//}
