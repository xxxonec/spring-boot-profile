package com.scnu.profile.module.scholat.service;


import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.ResultTop;

import java.util.List;

/**
 * Created by CodeGenerator on 2017/09/05.
 */
public interface ResultTopService extends Service<ResultTop> {
    /**
     * 总体机构文献排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllUnitPaperTop(Integer taskId);

    /**
     * 总体机构关注排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllUnitImpactTop(Integer taskId);

    /**
     * 总体期刊文献排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllJournalPaperTop(Integer taskId);

    /**
     * 总体期刊关注排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllJournalImpactTop(Integer taskId);

    /**
     * 总体学者文献排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllAuthorPaperTop(Integer taskId);

    /**
     * 总体学者关注排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllAuthorImpactTop(Integer taskId);

    /**
     * 总体文献关注排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllPaperImpactTop(Integer taskId);

    /**
     * 总体文献下载排名
     * @param taskId
     * @return
     */
    List<ResultTop> findAllPaperDownloadTop(Integer taskId);
}
