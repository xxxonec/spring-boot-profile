package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.mybatis.mapper.PaperMapper;
import com.scnu.profile.mybatis.mapper.UnitMapper;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.mybatis.model.Unit;
import com.scnu.profile.module.scholat.service.UnitService;
import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.utils.AddressUtils;
import com.scnu.profile.utils.ServiceUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
@Service
@Transactional
public class UnitServiceImpl extends AbstractService<Unit> implements UnitService {
    @Resource
    private UnitMapper unitMapper;
    @Resource
    private PaperMapper paperMapper;

    @Override
    public List<Paper> getAllPaper(String id) {
        return unitMapper.getAllPaper(id);
    }

    /**
     * 获取所有地址信息为空的unit
     *
     * @return
     */
    @Override
    public List<Unit> getAllNullAddress() {
        return unitMapper.getAllNullAddress();
    }

    @Override
    public void updateUnitInfo() {
        List<Unit> units = unitMapper.getAllNullAddress();
        for(Unit u : units){
            try {
                Map<String,String> map = AddressUtils.getAddress(u.getName());
                if(!map.isEmpty()){
                    u.setCategory(map.get("category"));
                    u.setAddress(map.get("address"));
                    u.setCity(map.get("city"));
                    u.setDistrict(map.get("district"));
                    u.setLatLng(map.get("lat")+","+map.get("lng"));
                    u.setProvince(map.get("province"));
                    u.setIsUpdated(1);
                    System.out.println(u.toString());
                    unitMapper.updateByPrimaryKey(u);
                }else {
                    u.setIsUpdated(1);
                    unitMapper.updateByPrimaryKey(u);
                    System.out.println(u.getName()+"-已爬取，但数据为空!!!");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println("update finished!!!");
    }
}
