package com.scnu.profile.module.scholat.service;


import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.HIndex;

/**
 * Created by CodeGenerator on 2017/09/12.
 */
public interface HIndexService extends Service<HIndex> {

}
