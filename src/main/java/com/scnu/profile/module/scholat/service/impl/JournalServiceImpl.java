package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.mybatis.mapper.AuthorMapper;
import com.scnu.profile.mybatis.mapper.JournalMapper;
import com.scnu.profile.mybatis.mapper.PaperMapper;
import com.scnu.profile.mybatis.mapper.UnitMapper;
import com.scnu.profile.mybatis.model.Journal;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.module.scholat.service.JournalService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
@Service
@Transactional
public class JournalServiceImpl extends AbstractService<Journal> implements JournalService {
    @Resource
    private JournalMapper journalMapper;
    @Resource
    private PaperMapper paperMapper;
    @Resource
    private AuthorMapper authorMapper;
    @Resource
    private UnitMapper unitMapper;

    @Override
    public List<Paper> getAllPaper(String id) {
        Paper paper = new Paper();
        paper.setJournalId(id);
        List<Paper> papers = paperMapper.select(paper);
        return papers;
    }

    @Override
    public List<Journal> getAllNullJournal() {
        return journalMapper.getAllNullJournal();
    }
}
