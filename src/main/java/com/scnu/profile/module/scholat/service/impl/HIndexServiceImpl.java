package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.scholat.service.HIndexService;
import com.scnu.profile.mybatis.mapper.HIndexMapper;
import com.scnu.profile.mybatis.model.HIndex;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/09/12.
 */
@Service
@Transactional
public class HIndexServiceImpl extends AbstractService<HIndex> implements HIndexService {
    @Resource
    private HIndexMapper hIndexMapper;

}
