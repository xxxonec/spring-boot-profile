package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.scholat.service.ResultTrendService;
import com.scnu.profile.mybatis.mapper.ResultTrendMapper;
import com.scnu.profile.mybatis.model.ResultTrend;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2017/09/05.
 */
@Service
@Transactional
public class ResultTrendServiceImpl extends AbstractService<ResultTrend> implements ResultTrendService {
    @Resource
    private ResultTrendMapper resultTrendMapper;

    /**
     * 总体文献趋势
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTrend> findAllYearTrend(Integer taskId) {
        return resultTrendMapper.findAllTrend(taskId,"总体文献趋势");
    }
}
