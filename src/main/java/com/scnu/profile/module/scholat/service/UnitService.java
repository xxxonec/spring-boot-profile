package com.scnu.profile.module.scholat.service;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.mybatis.model.Unit;
import com.scnu.profile.core.service.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
public interface UnitService extends Service<Unit> {
    /**
     * 获取该unit所有的paper
     * @param id
     * @return
     */
    List<Paper> getAllPaper(String id);

    /**
     * 获取所有地址信息为空的unit
     * @return
     */
    List<Unit> getAllNullAddress();

    void updateUnitInfo();
}
