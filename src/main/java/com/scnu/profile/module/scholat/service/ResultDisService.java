package com.scnu.profile.module.scholat.service;
import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.ResultDis;

import java.util.List;


/**
 * Created by CodeGenerator on 2017/09/05.
 */
public interface ResultDisService extends Service<ResultDis> {
    /**
     * 总体地域分布
     * @param taskId
     * @return
     */
    List<ResultDis> findAllProvinceDis(Integer taskId);

    /**
     * 总体学校分布
     * @param taskId
     * @return
     */
    List<ResultDis> findAllUnitDis(Integer taskId);

    /**
     * 总体学科分布
     * @param taskId
     * @return
     */
    List<ResultDis> findAllSubjectDis(Integer taskId);

    /**
     * 总体研究方向分布
     * @param taskId
     * @return
     */
    List<ResultDis> findAllKeywordDis(Integer taskId);
}
