package com.scnu.profile.module.scholat.service;
import com.scnu.profile.mybatis.model.Author;
import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.Paper;

import java.util.List;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
public interface AuthorService extends Service<Author> {
    /**
     * 获取该作者所有paper
     * @param id
     * @return
     */
    List<Paper> getAllPaper(String id);

    /**
     * 获取该作者所有paper的摘要
     * @param id
     * @return
     */
    String getAllSummary(String id);

    /**
     *获取该作者所有paper的关键词
     * @param id
     * @return
     */
    String getAllKeyword(String id);
}
