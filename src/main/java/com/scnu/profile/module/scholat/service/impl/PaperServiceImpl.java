package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.mybatis.mapper.PaperMapper;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.module.scholat.service.PaperService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
@Service
@Transactional
public class PaperServiceImpl extends AbstractService<Paper> implements PaperService {
    @Resource
    private PaperMapper paperMapper;

}
