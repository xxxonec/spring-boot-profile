package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.mybatis.mapper.AuthorMapper;
import com.scnu.profile.mybatis.model.Author;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.module.scholat.service.AuthorService;
import com.scnu.profile.core.mapper.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
@Service
@Transactional
public class AuthorServiceImpl extends AbstractService<Author> implements AuthorService {
    @Resource
    private AuthorMapper authorMapper;

    @Override
    public List<Paper> getAllPaper(String id) {
        return authorMapper.getAllPaper(id);
    }

    @Override
    public String getAllSummary(String id) {
        List<Paper> papers = authorMapper.getAllPaper(id);
        ArrayList summaryList = new ArrayList();
        for (Paper paper:papers){
            if (!"".equals(paper.getSummary())){
                summaryList.add(paper.getSummary());
            }

        }
        String summaryString = StringUtils.join(summaryList,"|");
        return summaryString;
    }

    @Override
    public String getAllKeyword(String id) {
        List<Paper> papers = authorMapper.getAllPaper(id);
        ArrayList keywordList = new ArrayList();
        for (Paper paper:papers){
            if (!"".equals(paper.getKeywords()))
                keywordList.add(paper.getKeywords());
        }
        String keywords = StringUtils.join(keywordList,",");
        return keywords;
    }
}
