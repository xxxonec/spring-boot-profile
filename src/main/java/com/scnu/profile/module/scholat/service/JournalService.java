package com.scnu.profile.module.scholat.service;
import com.scnu.profile.mybatis.model.Journal;
import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.Paper;

import java.util.List;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
public interface JournalService extends Service<Journal> {
    /**
     * 获取该期刊所有的paper
     * @param id
     * @return
     */
    List<Paper> getAllPaper(String id);

    List<Journal> getAllNullJournal();
}
