package com.scnu.profile.module.scholat.service;


import com.scnu.profile.core.service.Service;
import com.scnu.profile.mybatis.model.ResultTrend;

import java.util.List;

/**
 * Created by CodeGenerator on 2017/09/05.
 */
public interface ResultTrendService extends Service<ResultTrend> {
    /**
     * 总体文献趋势
     * @param taskId
     * @return
     */
    List<ResultTrend> findAllYearTrend(Integer taskId);
}
