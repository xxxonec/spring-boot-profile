package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.scholat.service.ResultDisService;
import com.scnu.profile.mybatis.mapper.ResultDisMapper;
import com.scnu.profile.mybatis.model.ResultDis;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2017/09/05.
 */
@Service
@Transactional
public class ResultDisServiceImpl extends AbstractService<ResultDis> implements ResultDisService {
    @Resource
    private ResultDisMapper resultDisMapper;

    /**
     * 总体地域分布
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultDis> findAllProvinceDis(Integer taskId) {
        return resultDisMapper.findAllDis(taskId,"总体地域分布");
    }

    /**
     * 总体学校分布
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultDis> findAllUnitDis(Integer taskId) {
        return resultDisMapper.findAllDis(taskId,"总体学校分布");
    }

    /**
     * 总体研究方向分布
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultDis> findAllKeywordDis(Integer taskId) {
        return resultDisMapper.findAllDis(taskId,"总体研究方向分布");
    }

    /**
     * 总体学科分布
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultDis> findAllSubjectDis(Integer taskId) {
        return resultDisMapper.findAllDis(taskId,"总体学科分布");
    }
}
