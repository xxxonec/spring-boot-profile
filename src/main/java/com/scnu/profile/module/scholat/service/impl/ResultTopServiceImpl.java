package com.scnu.profile.module.scholat.service.impl;

import com.scnu.profile.core.mapper.AbstractService;
import com.scnu.profile.module.scholat.service.ResultTopService;
import com.scnu.profile.mybatis.mapper.ResultTopMapper;
import com.scnu.profile.mybatis.model.ResultTop;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2017/09/05.
 */
@Service
@Transactional
public class ResultTopServiceImpl extends AbstractService<ResultTop> implements ResultTopService {
    @Resource
    private ResultTopMapper resultTopMapper;

    /**
     * 总体机构文献排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllUnitPaperTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体机构文献排名");
    }

    /**
     * 总体机构关注排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllUnitImpactTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体机构关注排名");
    }

    /**
     * 总体期刊文献排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllJournalPaperTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体期刊文献排名");
    }

    /**
     * 总体期刊关注排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllJournalImpactTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体期刊关注排名");
    }

    /**
     * 总体学者文献排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllAuthorPaperTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体学者文献排名");
    }

    /**
     * 总体学者关注排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllAuthorImpactTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体学者关注排名");
    }

    /**
     * 总体文献关注排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllPaperImpactTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体文献关注排名");
    }

    /**
     * 总体文献下载排名
     *
     * @param taskId
     * @return
     */
    @Override
    public List<ResultTop> findAllPaperDownloadTop(Integer taskId) {
        return resultTopMapper.findAllTop(taskId,"总体文献下载排名");
    }
}
