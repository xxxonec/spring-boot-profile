package com.scnu.profile.module.scholat.service;
import com.scnu.profile.mybatis.model.Label;
import com.scnu.profile.core.service.Service;


/**
 * Created by CodeGenerator on 2017/07/27.
 */
public interface LabelService extends Service<Label> {

    Boolean checkLabel(String labelName);

    Integer getIdFromName(String labelName);
}
