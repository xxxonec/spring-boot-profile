package com.scnu.profile.module.scholat.service;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.core.service.Service;


/**
 * Created by CodeGenerator on 2017/07/03.
 */
public interface PaperService extends Service<Paper> {

}
