package com.scnu.profile.module.auth.service;

import com.alibaba.fastjson.JSONObject;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.utils.JsonResult;

/**
 * Created by pcmmm on 17/8/15.
 */
public interface AuthService {
    Result register(User userToAdd);

    Result login(String username, String password);

    String refresh(String oldToken);
}
