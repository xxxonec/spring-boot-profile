package com.scnu.profile.module.auth.service.impl;

import com.alibaba.fastjson.JSON;
import com.scnu.profile.config.security.JwtTokenUtil;
import com.scnu.profile.config.security.JwtUser;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.core.result.ResultGenerator;
import com.scnu.profile.module.auth.vo.Token;
import com.scnu.profile.mybatis.mapper.UserRoleMapper;
import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.module.auth.service.AuthService;
import com.scnu.profile.module.system.service.RoleService;
import com.scnu.profile.module.system.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by pcmmm on 17/8/15.
 */
@Service
@SuppressWarnings("all")
public class AuthServiceImpl implements AuthService{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;
    private UserService userRepository;
    private RoleService roleRepository;
    private UserRoleMapper userRoleMapper;
    @Autowired
    public AuthServiceImpl(
            AuthenticationManager authenticationManager,
            UserDetailsService userDetailsService,
            JwtTokenUtil jwtTokenUtil,
            UserService userRepository,
            RoleService roleRepository,
            UserRoleMapper userRoleMapper) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRoleMapper = userRoleMapper;
    }

    @Override
    public Result register(User user) {
        User user1 = userRepository.findByUserName(user.getUsername());
        if (user1 != null){
            return ResultGenerator.genFailResult("账号已存在");
        }
        if (user.getUsername() == null) {
            return ResultGenerator.genFailResult("注册账号错误!");
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        final String rawPassword = user.getPassword();
        user.setPassword(encoder.encode(rawPassword));
        user.setLastPwdReset(new Date());
        user.setCreateTime(new Date());
        Role userRole = roleRepository.findByName("ROLE_USER");
        if (userRole == null){
            Long roleId = roleRepository.saveAndReturnId(new Role("ROLE_USER"));
            userRole = roleRepository.findBy("id",roleId);
        }
        userRepository.saveAndReturnId(user);
        userRoleMapper.addRole(user.getId(),userRole.getId());
        List<Role> roleList = userRoleMapper.getRoles(user.getId());
        List<String> rolesName = userRoleMapper.getRoles(user.getId()).stream().map(Role::getRoleName).collect(Collectors.toList());
        user.setRolesName(rolesName);
        try {
            return ResultGenerator.genSuccessResult(user,"注册成功!");
        } catch (DataIntegrityViolationException e) {
            logger.debug(e.getMessage());
            return ResultGenerator.genFailResult(e.getRootCause().getMessage());
        }
    }

    @Override
    public Result login(String username, String password) {

        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        try {
            final Authentication authentication = authenticationManager.authenticate(upToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            User u = userRepository.findByUserName(username);
            List<String> rolesName = userRoleMapper.getRoles(u.getId()).stream().map(Role::getRoleName).collect(Collectors.toList());
            u.setRolesName(rolesName);
            Map<String,Object> result = new HashMap<String,Object>();
            Token token = new Token(jwtTokenUtil.generateToken(userDetails),"Bearer");
            result.put("tokenObj",token);
            result.put("user",u);
            Object jsonObject = JSON.toJSON(result);
            return ResultGenerator.genSuccessResult(jsonObject,"登陆成功!");
        } catch (BadCredentialsException e) {
            logger.debug(e.getMessage());
            return ResultGenerator.genFailResult("帐号或密码错误");
        }
    }

    @Override
    public String refresh(String oldToken) {
        final String token = oldToken.substring(tokenHead.length());
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPwdReset())){
            return jwtTokenUtil.refreshToken(token);
        }
        return null;
    }
}
