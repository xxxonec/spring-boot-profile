package com.scnu.profile.module.crawler;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.*;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.HtmlBean;

import java.util.List;

/**
 * Created by pcmmm on 17/7/19.
 */
@Gecco(matchUrl="http://navi.cnki.net/knavi/JournalDetail?pcode={dbCode}&pykm={id}", pipelines="journalPipeline")
public class JournalDetailInfo implements HtmlBean {
    public static final String PAPER_NUM_RULE = "ul#publishInfo > li:eq(1) > p:eq(2) > span";
    public static final String QUOTE_NUM_RULE = "ul#publishInfo > li:eq(1) > p:eq(4) > span";
    public static final String DOWNLOAD_NUM_RULE = "ul#publishInfo > li:eq(1) > p:eq(3) > span";
    public static final String LEVELS_RULE = "p.journalType > span";
    public static final String PUB_CYCLE_RULE = "p:matchesOwn(^出版周期) > span";
    public static final String DETAIL_INFO_RULE = "ul#evaluateInfo > li > p , ul#evaluateInfo > li > p > span , ul#evaluateInfo > li > h4";
    @RequestParameter("id")
    private String id;

    private String name;

    private String enName;

    private String issn;

    @Text
    @HtmlField(cssPath = LEVELS_RULE)
    private List<String> levels;

    @RequestParameter("dbCode")
    private String dbCode;


    private String impactFactor;


    private String searchIndex;



    @Text
    @HtmlField(cssPath = PAPER_NUM_RULE)
    private String papersNum;

    @Text
    @HtmlField(cssPath = QUOTE_NUM_RULE)
    private String quoteNum;

    @Text
    @HtmlField(cssPath = DOWNLOAD_NUM_RULE)
    private String downloadNum;


    @Text
    @HtmlField(cssPath = PUB_CYCLE_RULE)
    private String pubCycle;

    @Text()
    @HtmlField(cssPath = DETAIL_INFO_RULE)
    private List<String> detailInfo;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public List<String> getLevels() {
        return levels;
    }

    public void setLevels(List<String> levels) {
        this.levels = levels;
    }

    public String getDbCode() {
        return dbCode;
    }

    public void setDbCode(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getImpactFactor() {
        return impactFactor;
    }

    public void setImpactFactor(String impactFactor) {
        this.impactFactor = impactFactor;
    }

    public String getSearchIndex() {
        return searchIndex;
    }

    public void setSearchIndex(String searchIndex) {
        this.searchIndex = searchIndex;
    }

    public String getPapersNum() {
        return papersNum;
    }

    public void setPapersNum(String papersNum) {
        this.papersNum = papersNum;
    }

    public String getQuoteNum() {
        return quoteNum;
    }

    public void setQuoteNum(String quoteNum) {
        this.quoteNum = quoteNum;
    }

    public String getPubCycle() {
        return pubCycle;
    }

    public void setPubCycle(String pubCycle) {
        this.pubCycle = pubCycle;
    }

    public List<String> getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(List<String> detailInfo) {
        this.detailInfo = detailInfo;
    }

    public String getDownloadNum() {
        return downloadNum;
    }

    public void setDownloadNum(String downloadNum) {
        this.downloadNum = downloadNum;
    }

    public static void crawl(HttpRequest request) {
        System.out.println("start-crawling");
        GeccoEngine.create()
                .classpath("com.scnu.profile.module.crawler")
                .start(request)
                .thread(1)
                .debug(true)
                .interval(2000)
                .start();


    }
}
