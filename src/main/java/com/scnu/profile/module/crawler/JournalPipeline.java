package com.scnu.profile.module.crawler;

import com.geccocrawler.gecco.annotation.PipelineName;
import com.geccocrawler.gecco.pipeline.Pipeline;
import com.scnu.profile.mybatis.model.Journal;
import com.scnu.profile.utils.ServiceUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by pcmmm on 17/7/19.
 */
@PipelineName("journalPipeline")
public class JournalPipeline implements Pipeline<JournalDetailInfo> {
//    @Override
//    public void process(SpiderBean spiderBean) {
//        //System.out.println(JSON.toJSONString(spiderBean));
//
//    }

    @Override
    public void process(JournalDetailInfo journalDetailInfo) {
        Journal journal = new Journal();
        Object[] levels = journalDetailInfo.getLevels().toArray();
        journal.setId(journalDetailInfo.getId());
        journal.setLevels(StringUtils.join(levels,","));
        journal.setPapersNum(journalDetailInfo.getPapersNum().split(" ")[0]);
        journal.setQuoteNum(journalDetailInfo.getQuoteNum().split(" ")[0]);
        journal.setPubCycle(journalDetailInfo.getPubCycle());
//        System.out.println(journal.toString());
        ServiceUtils.getServiceUtils().getJournalService().update(journal);
    }
}
