package com.scnu.profile.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.scnu.profile.config.security.JwtAuthenticationRequest;
import com.scnu.profile.config.security.JwtAuthenticationResponse;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.module.auth.service.AuthService;
import com.scnu.profile.utils.JsonResult;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by admin on 2017/8/16.
 */
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthService authService;

    @ApiOperation(value = "登陆")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "username",value = "用户名",required = true,dataType = "String"),
//            @ApiImplicitParam(name = "password",value = "密码",required = true,dataType = "String")
//    })
    @ApiImplicitParam(name = "authenticationRequest",value = "JWT登陆验证",required = true,dataType = "JwtAuthenticationRequest")
    @RequestMapping(value = "login",method = RequestMethod.POST)
    public Result createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException{
        return authService.login(authenticationRequest.getUsername(),authenticationRequest.getPassword());
    }


    @ApiOperation(value = "刷新Token")
    @ApiImplicitParam(name = "request", value = "请求信息（带有tokenHeader）", required = true,
            dataType = "HttpServletRequest")
    @RequestMapping(value = "/refresh",method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) throws AuthenticationException{
        String token = request.getHeader(tokenHeader);
        String refreshedToken = authService.refresh(token);
        if (refreshedToken == null){
            return ResponseEntity.badRequest().body(null);
        }else {
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        }
    }

    @ApiOperation(value = "注册")
    @ApiImplicitParam(name = "addedUser", value = "用户实体", required = true, dataType = "User")
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public Result register(@RequestBody User user) throws AuthenticationException{
//        User user = new User();
//        user.setUsername(username);
//        user.setPassword(password);
        return authService.register(user);
    }
}
