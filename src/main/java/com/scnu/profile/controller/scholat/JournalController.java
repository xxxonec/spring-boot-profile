package com.scnu.profile.controller.scholat;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.core.result.ResultGenerator;
import com.scnu.profile.mybatis.model.Journal;
import com.scnu.profile.module.scholat.service.JournalService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2017/07/03.
*/
@RestController
@RequestMapping("/sys/journal")
public class JournalController {
    @Resource
    private JournalService journalService;

    //@ApiOperation(value="创建journal", notes="根据jouranl对象创建jouranl")
    //@ApiImplicitParams({
    //        @ApiImplicitParam(dataType = "Journal", name = "journal", value = "journal信息", required = true)
    //})
    @PostMapping("/add")
    public Result add(Journal journal) {
        journalService.save(journal);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        journalService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/update")
    public Result update(Journal journal) {
        journalService.update(journal);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/detail")
    public Result detail(Integer id) {
        Journal journal = journalService.findById(id);
        return ResultGenerator.genSuccessResult(journal);
    }

    @PostMapping("/list")
    public Result list(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Journal> list = journalService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
