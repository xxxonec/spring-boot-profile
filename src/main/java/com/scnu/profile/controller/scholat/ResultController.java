package com.scnu.profile.controller.scholat;

import com.alibaba.fastjson.JSON;
import com.feilong.core.util.AggregateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.scnu.profile.config.security.JwtAuthenticationResponse;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.core.result.ResultGenerator;
import com.scnu.profile.module.scholat.service.*;
import com.scnu.profile.mybatis.mapper.*;
import com.scnu.profile.mybatis.model.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by pcmmm on 17/9/5.
 */
@RestController
@RequestMapping("sys/macro/profile")
public class ResultController {
    @Resource
    private AuthorService authorService;
    @Resource
    private ResultDisService resultDisService;
    @Resource
    private ResultTopService resultTopService;
    @Resource
    private ResultTrendService resultTrendService;
    @Resource
    private ResultDisMapper resultDisMapper;
    @Resource
    private ResultTopMapper resultTopMapper;
    @Resource
    private ResultTrendMapper resultTrendMapper;
    @Resource
    private UnitMapper unitMapper;
    @Resource
    private PaperMapper paperMapper;
    @Resource
    private AuthorMapper authorMapper;
    @Resource
    private JournalMapper journalMapper;

    @Resource
    private HIndexService hIndexService;

    @ApiOperation(value = "获取资源分布")
    @GetMapping("/dis/resource")
    public Result getAllResourceCount(){
        Integer paperCount = paperMapper.selectCount(null);
        Integer unitCount = unitMapper.selectCount(null);
        Integer jCount = journalMapper.selectCount(null);
        Integer aCount = authorMapper.selectCount(null);
        Map<String,Integer> maps = new HashMap<>();
        maps.put("papercount",paperCount);
        maps.put("unitcount",unitCount);
        maps.put("journalcount",jCount);
        maps.put("authorcount",aCount);
        return ResultGenerator.genSuccessResult(maps);
    }

    @ApiOperation(value = "获取总体地域分布")
    @GetMapping("/dis/province")
    public Result getAllProvinceDis()  {
        Integer taskId = 1;
        List<ResultDis> list = resultDisService.findAllProvinceDis(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体机构分布")
    @GetMapping("/dis/unit")
    public Result getAllUnitDis()  {
        Integer taskId = 1;
        List<ResultDis> list = resultDisService.findAllUnitDis(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体学科分布")
    @GetMapping("/dis/subject")
    public Result getAllSubjectDis() {
        Integer taskId = 1;
        List<ResultDis> list = resultDisService.findAllSubjectDis(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体研究方向分布")
    @GetMapping("/dis/keywords")
    public Result getAllKeywordsDis() {
        Integer taskId = 1;
        List<ResultDis> list = resultDisService.findAllKeywordDis(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体文献趋势")
    @GetMapping("/trend/paper")
    public Result getAllPaperTrend() {
        Integer taskId = 1;
        List<ResultTrend> list = resultTrendService.findAllYearTrend(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体文献排名")
    @GetMapping("/top/paper")
    public Result getAllPaperCountAndImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllPaperDownloadTop(taskId);
        List<ResultTop> list2 = resultTopService.findAllPaperImpactTop(taskId);
        Map<String,List<ResultTop>> map = new HashMap<>();
        map.put("down",list);
        map.put("quote",list2);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取总体影响力排名")
    @GetMapping("/top/paper/impact")
    public Result getAllPaperImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllPaperImpactTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体下载排名")
    @GetMapping("/top/paper/down")
    public Result getAllPaperDownTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllPaperDownloadTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体作者排名")
    @GetMapping("/top/author")
    public Result getAllAuthorCountAndImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllAuthorPaperTop(taskId);
        List<ResultTop> list2 = resultTopService.findAllAuthorImpactTop(taskId);
        Map<String,List<ResultTop>> map = new HashMap<>();
        map.put("amount",list);
        map.put("quote",list2);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取总体作者文献排名")
    @GetMapping("/top/author/count")
    public Result getAllAuthorCountTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllAuthorPaperTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体作者文献影响力排名")
    @GetMapping("/top/author/impact")
    public Result getAllAuthorImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllAuthorImpactTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体机构排名")
    @GetMapping("/top/unit")
    public Result getAllUnitCountAndImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllUnitImpactTop(taskId);
        List<ResultTop> list2 = resultTopService.findAllUnitPaperTop(taskId);
        Map<String,List<ResultTop>> map = new HashMap<>();
        map.put("quote",list);
        map.put("amount",list2);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取总体机构文献影响力排名")
    @GetMapping("/top/unit/impact")
    public Result getAllUnitImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllUnitImpactTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体机构文献排名")
    @GetMapping("/top/unit/count")
    public Result getAllUnitPaperTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllUnitPaperTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体期刊排名")
    @GetMapping("/top/journal")
    public Result getAllJournalCountAndImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllJournalImpactTop(taskId);
        List<ResultTop> list2 = resultTopService.findAllJournalPaperTop(taskId);
        Map<String,List<ResultTop>> map = new HashMap<>();
        map.put("quote",list);
        map.put("amount",list2);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取总体期刊文献影响力排名")
    @GetMapping("/top/journal/impact")
    public Result getAllJournalImpactTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllJournalImpactTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "获取总体期刊文献排名")
    @GetMapping("/top/journal/count")
    public Result getAllJournalPaperTop() {
        Integer taskId = 1;
        List<ResultTop> list = resultTopService.findAllJournalPaperTop(taskId);
        return ResultGenerator.genSuccessResult(list);
    }





    @ApiOperation(value = "获取个人信息")
    @GetMapping("query/name/{username}")
    public Result getAuthorProfile(@PathVariable String username){
        Author author = new Author();
        author.setName(username);
        List<Author> authors = authorMapper.select(author);
        Map<String,Object> map = new HashMap<>();
        if (authors.size()>0){
            Author author1 = authors.get(0);
//            ResultTrend resultTrend = new ResultTrend();
            ResultDis resultDis = new ResultDis();
//            resultTrend.setResultName("个人文献趋势");
//            resultTrend.setrId(author1.getId());
//            resultTrend.setrName(author1.getName());
            resultDis.setrId(author1.getId());
            resultDis.setrName(author1.getName());
            List<ResultTrend> resultDises1 = resultTrendMapper.findAuthorTrend(1,"个人文献趋势",author1.getId(),author1.getName());
            resultDis.setResultName("个人期刊分布");
            List<ResultDis> resultDises2 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人来源分布");
            List<ResultDis> resultDises3 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人关键词分布");
            List<ResultDis> resultDises4 = resultDisMapper.select(resultDis);
            Integer hIndex = hIndexService.findBy("rId",author1.getId()).gethIndex();
            Author author2 = authorService.findBy("id",author1.getId());
            List<Paper> papers = authorMapper.getTop5Paper(author1.getId());
            List<Paper> allPapers = authorMapper.getAllPaper(author1.getId());
            Map<String,String> map1 = new HashMap<>();
            map1.put("id",author2.getId());
            map1.put("name",author2.getName());
            map1.put("unit",author2.getUnitName());
            map1.put("domains",author2.getDomains());
            map1.put("hIndex",String.valueOf(hIndex));
            map1.put("count",String.valueOf(allPapers.size()));
            map.put("info",map1);
            map.put("top5",papers);
            map.put("paper",resultDises1);
            map.put("journal",resultDises2);
            map.put("source",resultDises3);
            map.put("keyword",resultDises4);
        }
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取个人信息")
    @GetMapping("query/{id}")
    public Result getAuthorProfileById(@PathVariable String id){
        Author author = new Author();
        author.setId(id);
        List<Author> authors = authorMapper.select(author);
        Map<String,Object> map = new HashMap<>();
        if (authors.size()>0){
            Author author1 = authors.get(0);
            ResultTrend resultTrend = new ResultTrend();
            ResultDis resultDis = new ResultDis();
            resultTrend.setResultName("个人文献趋势");
            resultTrend.setrId(author1.getId());
            resultTrend.setrName(author1.getName());
            resultDis.setrId(author1.getId());
            resultDis.setrName(author1.getName());
            List<ResultTrend> resultDises1 = resultTrendMapper.select(resultTrend);
            resultDis.setResultName("个人期刊分布");
            List<ResultDis> resultDises2 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人来源分布");
            List<ResultDis> resultDises3 = resultDisMapper.select(resultDis);
            resultDis.setResultName("个人关键词分布");
            List<ResultDis> resultDises4 = resultDisMapper.select(resultDis);
            Integer hIndex = hIndexService.findBy("rId",author1.getId()).gethIndex();
            Author author2 = authorService.findBy("id",author1.getId());
            List<Paper> papers = authorMapper.getTop5Paper(author1.getId());
            Map<String,String> map1 = new HashMap<>();
            map1.put("id",author2.getId());
            map1.put("name",author2.getName());
            map1.put("unit",author2.getUnitName());
            map1.put("domains",author2.getDomains());
            map1.put("hIndex",String.valueOf(hIndex));
            map.put("info",map1);
            map.put("top5",papers);
            map.put("paper",resultDises1);
            map.put("journal",resultDises2);
            map.put("source",resultDises3);
            map.put("keyword",resultDises4);
        }
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation(value = "获取关系")
    @GetMapping("query/relation/{id}")
    public Result getRelations(@PathVariable String id){
        Author author1 = authorService.findBy("id",id);
        List<Paper> papers = authorMapper.getAllPaper(author1.getId());
        List<Author> ids = new ArrayList<>();
        Set<String> categorysSet = new HashSet<>();
        for(Paper paper: papers){
            String[] aids = paper.getAuthorIds().split(",");
            Author author2 = new Author();
            for (int i=0 ;i<aids.length;i++){
                if (aids[i]!=author1.getId()) {
                    author2.setId(aids[i]);
                    ids.add(author2);
                }
                categorysSet.add(authorService.findBy("id",aids[i]).getUnitName());
            }
        }
        List<String> categorys = new ArrayList<>();
        categorys.addAll(categorysSet);
        Map<String,Integer> sortMap = AggregateUtil.groupCount(ids,"id");
//            System.out.println(JSON.toJSONString(sortMap));
        List<Relation> relations = new ArrayList<>();
        List<Node> nodes = new ArrayList<>();
        List<String> exisName = new ArrayList<>();
        List<String> exisId = new ArrayList<>();
        for (String idd:sortMap.keySet()){
            Author targetAuthor = authorService.findBy("id",idd);
            String targetName = targetAuthor.getName();
            if (!exisName.contains(targetName)){
                exisName.add(targetName);
                exisId.add(idd);
            }
        }
        for (String id2:exisId){
            Author targetAuthor = authorService.findBy("id",id2);
            String targetName = targetAuthor.getName();
            String targetUnit = targetAuthor.getUnitName();
            if(!author1.getName().equals(targetName)){
                Relation relation = new Relation(author1.getName(),targetName,sortMap.get(id2));
                Node node = new Node(targetName,targetUnit,categorys.indexOf(targetUnit));
                relations.add(relation);
                nodes.add(node);
            }else {
                Node node = new Node(targetName,targetUnit,categorys.indexOf(targetUnit));
                nodes.add(node);
            }

        }
        HashMap<String,Object> map1 = new HashMap<>();
        map1.put("categories",categorys);
        map1.put("links",relations);
        map1.put("nodes",nodes);
        return ResultGenerator.genSuccessResult(map1);
    }

}
