package com.scnu.profile.controller.scholat;
import com.scnu.profile.core.result.Result;
import com.scnu.profile.core.result.ResultGenerator;
import com.scnu.profile.mybatis.model.Unit;
import com.scnu.profile.module.scholat.service.UnitService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2017/07/03.
*/
@RestController
@RequestMapping("/sys/unit")
public class UnitController {
    @Resource
    private UnitService unitService;

    @PostMapping("/add")
    public Result add(Unit unit) {
        unitService.save(unit);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        unitService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/update")
    public Result update(Unit unit) {
        unitService.update(unit);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/detail")
    public Result detail(Integer id) {
        Unit unit = unitService.findById(id);
        return ResultGenerator.genSuccessResult(unit);
    }

    @PostMapping("/list")
    public Result list(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Unit> list = unitService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @GetMapping("/updateInfo")
    public Result updateDetailInfo(){
        unitService.updateUnitInfo();
        return ResultGenerator.genSuccessResult();
    }
}
