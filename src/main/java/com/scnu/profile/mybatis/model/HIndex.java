package com.scnu.profile.mybatis.model;

import javax.persistence.*;

@Table(name = "h_index")
public class HIndex {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "r_id")
    private String rId;

    @Column(name = "r_name")
    private String rName;

    @Column(name = "h_index")
    private Integer hIndex;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return task_id
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return r_id
     */
    public String getrId() {
        return rId;
    }

    /**
     * @param rId
     */
    public void setrId(String rId) {
        this.rId = rId;
    }

    /**
     * @return r_name
     */
    public String getrName() {
        return rName;
    }

    /**
     * @param rName
     */
    public void setrName(String rName) {
        this.rName = rName;
    }

    /**
     * @return h_index
     */
    public Integer gethIndex() {
        return hIndex;
    }

    /**
     * @param hIndex
     */
    public void sethIndex(Integer hIndex) {
        this.hIndex = hIndex;
    }
}