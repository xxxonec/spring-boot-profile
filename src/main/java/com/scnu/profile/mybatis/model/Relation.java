package com.scnu.profile.mybatis.model;

/**
 * Created by pcmmm on 17/9/14.
 */
public class Relation {
    private String source;
    private String target;
    private Integer value;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Relation(String source, String target, Integer value) {
        this.source = source;
        this.target = target;
        this.value = value;
    }

    public Relation() {
    }

    @Override
    public String toString() {
        return "Relation{" +
                "source='" + source + '\'' +
                ", target='" + target + '\'' +
                ", value=" + value +
                '}';
    }
}
