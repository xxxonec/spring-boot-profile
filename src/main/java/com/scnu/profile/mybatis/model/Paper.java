package com.scnu.profile.mybatis.model;

import javax.persistence.*;

public class Paper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String title;

    @Column(name = "journal_id")
    private String journalId;

    @Column(name = "journal_name")
    private String journalName;

    @Column(name = "pub_date")
    private String pubDate;

    private String doi;

    @Column(name = "db_code")
    private String dbCode;

    @Column(name = "db_name")
    private String dbName;

    private String categorys;

    private String keywords;

    private String funds;

    @Column(name = "quote_num")
    private Long quoteNum;

    @Column(name = "quote2_num")
    private Long quote2Num;

    @Column(name = "download_num")
    private Long downloadNum;

    private String url;

    private String summary;

    @Column(name = "author_ids")
    private String authorIds;

    @Column(name = "author_names")
    private String authorNames;

    @Column(name = "org_names")
    private String orgNames;

    @Column(name = "org_ids")
    private String orgIds;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return journal_id
     */
    public String getJournalId() {
        return journalId;
    }

    /**
     * @param journalId
     */
    public void setJournalId(String journalId) {
        this.journalId = journalId;
    }

    /**
     * @return journal_name
     */
    public String getJournalName() {
        return journalName;
    }

    /**
     * @param journalName
     */
    public void setJournalName(String journalName) {
        this.journalName = journalName;
    }

    /**
     * @return pub_date
     */
    public String getPubDate() {
        return pubDate;
    }

    /**
     * @param pubDate
     */
    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    /**
     * @return doi
     */
    public String getDoi() {
        return doi;
    }

    /**
     * @param doi
     */
    public void setDoi(String doi) {
        this.doi = doi;
    }

    /**
     * @return db_code
     */
    public String getDbCode() {
        return dbCode;
    }

    /**
     * @param dbCode
     */
    public void setDbCode(String dbCode) {
        this.dbCode = dbCode;
    }

    /**
     * @return db_name
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * @param dbName
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * @return categorys
     */
    public String getCategorys() {
        return categorys;
    }

    /**
     * @param categorys
     */
    public void setCategorys(String categorys) {
        this.categorys = categorys;
    }

    /**
     * @return keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * @return funds
     */
    public String getFunds() {
        return funds;
    }

    /**
     * @param funds
     */
    public void setFunds(String funds) {
        this.funds = funds;
    }

    /**
     * @return quote_num
     */
    public Long getQuoteNum() {
        return quoteNum;
    }

    /**
     * @param quoteNum
     */
    public void setQuoteNum(Long quoteNum) {
        this.quoteNum = quoteNum;
    }

    /**
     * @return quote2_num
     */
    public Long getQuote2Num() {
        return quote2Num;
    }

    /**
     * @param quote2Num
     */
    public void setQuote2Num(Long quote2Num) {
        this.quote2Num = quote2Num;
    }

    /**
     * @return download_num
     */
    public Long getDownloadNum() {
        return downloadNum;
    }

    /**
     * @param downloadNum
     */
    public void setDownloadNum(Long downloadNum) {
        this.downloadNum = downloadNum;
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return author_ids
     */
    public String getAuthorIds() {
        return authorIds;
    }

    /**
     * @param authorIds
     */
    public void setAuthorIds(String authorIds) {
        this.authorIds = authorIds;
    }

    /**
     * @return author_names
     */
    public String getAuthorNames() {
        return authorNames;
    }

    /**
     * @param authorNames
     */
    public void setAuthorNames(String authorNames) {
        this.authorNames = authorNames;
    }

    /**
     * @return org_names
     */
    public String getOrgNames() {
        return orgNames;
    }

    /**
     * @param orgNames
     */
    public void setOrgNames(String orgNames) {
        this.orgNames = orgNames;
    }

    /**
     * @return org_ids
     */
    public String getOrgIds() {
        return orgIds;
    }

    /**
     * @param orgIds
     */
    public void setOrgIds(String orgIds) {
        this.orgIds = orgIds;
    }

    public String toTxt(){
        String _summary = summary;
        String _title = title;
        String _funds = funds;
        if (summary != null &&!"".equals(summary)){
            _summary = summary.replace("|",",");
        }
        if(title != null &&!"".equals(title)){
            _title = title.replace("|"," ");
        }
        if(funds != null && !"".equals(funds)){
            _funds = funds.replace("|","-");
        }
        return id + "|" +
                _title + "|" +
                _summary + "|" +
                journalId + "|" +
                pubDate + "|" +
                categorys + "|" +
                authorIds + "|" +
                orgIds + "|" +
                keywords + "|" +
                _funds + "|" +
                quoteNum + "|" +
                quote2Num + "|" +
                downloadNum + "\n";
    }

    @Override
    public String toString() {
        return "Paper{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", journalId='" + journalId + '\'' +
                ", journalName='" + journalName + '\'' +
                ", pubDate='" + pubDate + '\'' +
                ", doi='" + doi + '\'' +
                ", dbCode='" + dbCode + '\'' +
                ", dbName='" + dbName + '\'' +
                ", categorys='" + categorys + '\'' +
                ", keywords='" + keywords + '\'' +
                ", funds='" + funds + '\'' +
                ", quoteNum=" + quoteNum +
                ", quote2Num=" + quote2Num +
                ", downloadNum=" + downloadNum +
                ", url='" + url + '\'' +
                ", summary='" + summary + '\'' +
                ", authorIds='" + authorIds + '\'' +
                ", authorNames='" + authorNames + '\'' +
                ", orgNames='" + orgNames + '\'' +
                ", orgIds='" + orgIds + '\'' +
                '}';
    }
}