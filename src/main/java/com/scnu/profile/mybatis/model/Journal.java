package com.scnu.profile.mybatis.model;

import javax.persistence.*;

public class Journal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String name;

    @Column(name = "en_name")
    private String enName;

    private String issn;

    private String levels;

    @Column(name = "db_code")
    private String dbCode;

    @Column(name = "impact_factor")
    private String impactFactor;

    @Column(name = "search_index")
    private String searchIndex;

    @Column(name = "papers_num")
    private String papersNum;

    @Column(name = "quote_num")
    private String quoteNum;

    @Column(name = "pub_cycle")
    private String pubCycle;
    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    /**
     * @return issn
     */
    public String getIssn() {
        return issn;
    }

    /**
     * @param issn
     */
    public void setIssn(String issn) {
        this.issn = issn;
    }

    /**
     * @return levels
     */
    public String getLevels() {
        return levels;
    }

    /**
     * @param levels
     */
    public void setLevels(String levels) {
        this.levels = levels;
    }

    /**
     * @return db_code
     */
    public String getDbCode() {
        return dbCode;
    }

    /**
     * @param dbCode
     */
    public void setDbCode(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getImpactFactor() {
        return impactFactor;
    }

    public void setImpactFactor(String impactFactor) {
        this.impactFactor = impactFactor;
    }

    public String getSearchIndex() {
        return searchIndex;
    }

    public void setSearchIndex(String searchIndex) {
        this.searchIndex = searchIndex;
    }

    public String getPapersNum() {
        return papersNum;
    }

    public void setPapersNum(String papersNum) {
        this.papersNum = papersNum;
    }

    public String getQuoteNum() {
        return quoteNum;
    }

    public void setQuoteNum(String quoteNum) {
        this.quoteNum = quoteNum;
    }

    public String getPubCycle() {
        return pubCycle;
    }

    public void setPubCycle(String pubCycle) {
        this.pubCycle = pubCycle;
    }


    public String toTxt() {
        String _levels = levels;
        String _papersNum = papersNum;
        String _quoteNum = quoteNum;
        String _pubCycle = pubCycle;
        if (_levels == null){
            _levels="";
        }
        if (_papersNum == null){
            _papersNum="";
        }
        if (_quoteNum == null){
            _quoteNum="";
        }
        if (_pubCycle == null){
            _pubCycle="";
        }
        return id + "|" +
                name + "|" +
                _levels + "|" +
                _papersNum + "|" +
                _quoteNum + "|" +
                _pubCycle + "\n";
    }

    @Override
    public String toString() {
        return "Journal{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", enName='" + enName + '\'' +
                ", issn='" + issn + '\'' +
                ", levels='" + levels + '\'' +
                ", dbCode='" + dbCode + '\'' +
                ", impactFactor='" + impactFactor + '\'' +
                ", searchIndex='" + searchIndex + '\'' +
                ", papersNum='" + papersNum + '\'' +
                ", quoteNum='" + quoteNum + '\'' +
                ", pubCycle='" + pubCycle + '\'' +
                '}';
    }
}