package com.scnu.profile.mybatis.model;

import java.util.Date;
import javax.persistence.*;

public class Label {
    @Id
    @Column(name = "label_id")
    private Integer labelId;

    @Column(name = "label_name")
    private String labelName;

    @Column(name = "father_node")
    private Integer fatherNode;

    @Column(name = "label_category")
    private Integer labelCategory;

    @Column(name = "label_type")
    private Integer labelType;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "label_state")
    private Integer labelState;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "modify_by")
    private String modifyBy;

    @Column(name = "label_dec")
    private String labelDec;

    /**
     * @return label_id
     */
    public Integer getLabelId() {
        return labelId;
    }

    /**
     * @param labelId
     */
    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    /**
     * @return label_name
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     * @param labelName
     */
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    /**
     * @return father_node
     */
    public Integer getFatherNode() {
        return fatherNode;
    }

    /**
     * @param fatherNode
     */
    public void setFatherNode(Integer fatherNode) {
        this.fatherNode = fatherNode;
    }

    /**
     * @return label_category
     */
    public Integer getLabelCategory() {
        return labelCategory;
    }

    /**
     * @param labelCategory
     */
    public void setLabelCategory(Integer labelCategory) {
        this.labelCategory = labelCategory;
    }

    /**
     * @return label_type
     */
    public Integer getLabelType() {
        return labelType;
    }

    /**
     * @param labelType
     */
    public void setLabelType(Integer labelType) {
        this.labelType = labelType;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return label_state
     */
    public Integer getLabelState() {
        return labelState;
    }

    /**
     * @param labelState
     */
    public void setLabelState(Integer labelState) {
        this.labelState = labelState;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * @return modify_by
     */
    public String getModifyBy() {
        return modifyBy;
    }

    /**
     * @param modifyBy
     */
    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    /**
     * @return label_dec
     */
    public String getLabelDec() {
        return labelDec;
    }

    /**
     * @param labelDec
     */
    public void setLabelDec(String labelDec) {
        this.labelDec = labelDec;
    }

    private Label(LabelBuilder builder) {
        this.labelName = builder.labelName;
        this.fatherNode = builder.fatherNode;
        this.createBy = builder.createBy;
        this.createTime = builder.createTime;
        this.labelCategory = builder.labelCategory;
        this.labelDec = builder.labelDec;
        this.labelState = builder.labelState;
        this.labelType = builder.labelType;
        this.modifyBy = builder.modifyBy;
        this.updateTime = builder.updateTime;
    }

    public Label() {
    }

    public static class LabelBuilder {

        private final String labelName;

        private final Integer fatherNode;

        private Integer labelCategory;

        private  Integer labelType;

        private  Date createTime;

        private  Integer labelState;

        private Date updateTime;

        private  String createBy;

        private  String modifyBy;

        private String labelDec;

        public LabelBuilder(String labelName, Integer fatherNode) {
            this.labelName = labelName;
            this.fatherNode = fatherNode;
        }

        public LabelBuilder createBy(String createBy) {
            this.createBy = createBy;
            return this;
        }

        public LabelBuilder createTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public LabelBuilder labelCategory(Integer labelCategory) {
            this.labelCategory = labelCategory;
            return this;
        }

        public LabelBuilder labelType(Integer labelType) {
            this.labelType = labelType;
            return this;
        }

        public LabelBuilder updateTime(Date updateTime) {
            this.updateTime = updateTime;
            return this;
        }

        public LabelBuilder modifyBy(String modifyBy) {
            this.modifyBy = modifyBy;
            return this;
        }
        public LabelBuilder labelDec(String labelDec) {
            this.labelDec = labelDec;
            return this;
        }

        public LabelBuilder labelState(Integer labelState) {
            this.labelState = labelState;
            return this;
        }

        public Label build() {
            return new Label(this);
        }
    }
}