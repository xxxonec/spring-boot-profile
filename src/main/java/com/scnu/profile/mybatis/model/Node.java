package com.scnu.profile.mybatis.model;

/**
 * Created by pcmmm on 17/9/14.
 */
public class Node {
    private String name;
    private String value;
    private Integer category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Node(String name, String value, Integer category) {
        this.name = name;
        this.value = value;
        this.category = category;
    }

    public Node() {
    }
}
