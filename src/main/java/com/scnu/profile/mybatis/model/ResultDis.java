package com.scnu.profile.mybatis.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;

@Table(name = "result_dis")
public class ResultDis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JSONField(serialize = false)
    private Integer id;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "result_name")
    @JSONField(name = "title")
    private String resultName;

    @Column(name = "r_id")
    @JSONField(name = "id")
    private String rId;

    @Column(name = "r_name")
    @JSONField(name = "name")
    private String rName;
    @Column(name = "j_id")
    @JSONField(name = "fieldId")
    private String jId;

    @Column(name = "j_name")
    @JSONField(name = "fieldName")
    private String jName;

    @JSONField(name = "value")
    private Long count;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return task_id
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return result_name
     */
    public String getResultName() {
        return resultName;
    }

    /**
     * @param resultName
     */
    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    /**
     * @return r_id
     */
    public String getrId() {
        return rId;
    }

    /**
     * @param rId
     */
    public void setrId(String rId) {
        this.rId = rId;
    }

    /**
     * @return r_name
     */
    public String getrName() {
        return rName;
    }

    /**
     * @param rName
     */
    public void setrName(String rName) {
        this.rName = rName;
    }

    /**
     * @return count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(Long count) {
        this.count = count;
    }

    public String getjId() {
        return jId;
    }

    public void setjId(String jId) {
        this.jId = jId;
    }

    public String getjName() {
        return jName;
    }

    public void setjName(String jName) {
        this.jName = jName;
    }
}