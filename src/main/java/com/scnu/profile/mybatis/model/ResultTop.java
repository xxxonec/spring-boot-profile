package com.scnu.profile.mybatis.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;

@Table(name = "result_top")
public class ResultTop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JSONField(serialize = false)
    private Integer id;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "result_name")
    @JSONField(name = "title")
    private String resultName;

    @Column(name = "r_id")
    @JSONField(name = "id")
    private String rId;

    @Column(name = "r_name")
    @JSONField(name = "name")
    private String rName;

    @Column(name = "paper_count")
    private Long paperCount;

    @Column(name = "quote_num")
    private Long quoteNum;

    @Column(name = "quote2_num")
    private Long quote2Num;

    @Column(name = "download_num")
    private Long downloadNum;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return task_id
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return result_name
     */
    public String getResultName() {
        return resultName;
    }

    /**
     * @param resultName
     */
    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    /**
     * @return r_id
     */
    public String getrId() {
        return rId;
    }

    /**
     * @param rId
     */
    public void setrId(String rId) {
        this.rId = rId;
    }

    /**
     * @return r_name
     */
    public String getrName() {
        return rName;
    }

    /**
     * @param rName
     */
    public void setrName(String rName) {
        this.rName = rName;
    }

    /**
     * @return paper_count
     */
    public Long getPaperCount() {
        return paperCount;
    }

    /**
     * @param paperCount
     */
    public void setPaperCount(Long paperCount) {
        this.paperCount = paperCount;
    }

    /**
     * @return quote_num
     */
    public Long getQuoteNum() {
        return quoteNum;
    }

    /**
     * @param quoteNum
     */
    public void setQuoteNum(Long quoteNum) {
        this.quoteNum = quoteNum;
    }

    /**
     * @return quote2_num
     */
    public Long getQuote2Num() {
        return quote2Num;
    }

    /**
     * @param quote2Num
     */
    public void setQuote2Num(Long quote2Num) {
        this.quote2Num = quote2Num;
    }

    /**
     * @return download_num
     */
    public Long getDownloadNum() {
        return downloadNum;
    }

    /**
     * @param downloadNum
     */
    public void setDownloadNum(Long downloadNum) {
        this.downloadNum = downloadNum;
    }
}