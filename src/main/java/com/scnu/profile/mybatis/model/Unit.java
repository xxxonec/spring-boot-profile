package com.scnu.profile.mybatis.model;

import javax.persistence.*;

public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String name;

    private String url;

    private String address;

    private String province;

    private String city;

    private String district;

    private String category;

    @Column(name = "lat_lng")
    private String latLng;

    private String rank;

    @Column(name = "is_updated")
    private Integer isUpdated;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }



    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLatLng() {
        return latLng;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(Integer isUpdated) {
        this.isUpdated = isUpdated;
    }

    /*
    StructField("id", StringType, true),
                StructField("name", StringType, true),
                StructField("province", StringType, true),
                StructField("city", StringType, true),
                StructField("district", StringType, true),
                StructField("lat_lng", StringType, true),
                StructField("category", StringType, true),
                StructField("lat_lng", StringType, true)
     */


    public String toTxt() {
        String _province = province;
        String _city = city;
        String _district = district;
        String _latLng = latLng;
        String _category = category;
        String _rank = rank;
        if(_province == null){
            _province = "";
        }
        if(_city == null){
            _city = "";
        }
        if(_district == null){
            _district = "";
        }
        if(_latLng == null){
            _latLng = "";
        }
        if(_category == null){
            _category = "";
        }
        if(_rank == null){
            _rank = "";
        }
        return id + "|" +
                name + "|" +
                _province  + "|" +
                _city  + "|" +
                _district  + "|" +
                _latLng  + "|" +
                _category  + "|" +
                _rank + "|"+"\n";
    }

    @Override
    public String toString() {
        return "Unit{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", address='" + address + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", category='" + category + '\'' +
                ", latLng='" + latLng + '\'' +
                ", rank='" + rank + '\'' +
                ", isUpdated=" + isUpdated +
                '}';
    }
}