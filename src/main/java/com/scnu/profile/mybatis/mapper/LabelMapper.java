package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Label;

import java.util.List;

public interface LabelMapper extends Mapper<Label> {
    Integer insertAndGetId(Label label);
    List<Label> getChildLabel (Integer labelId);
    Label getFatherLabel(Integer labelId);
    Integer getFatherLabelCount();
    Integer getChrildLabelCount(Integer labelId);
}