package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Journal;

import java.util.List;

public interface JournalMapper extends Mapper<Journal> {
    void updateDetailInfo();
    List<Journal> getAllNullJournal();
}