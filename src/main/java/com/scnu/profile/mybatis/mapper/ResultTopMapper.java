package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.ResultTop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import javax.jdo.annotations.Query;
import java.util.List;

public interface ResultTopMapper extends Mapper<ResultTop> {
    @Select("SELECT " +
            "id," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "paper_count AS paperCount," +
            "quote_num AS quoteNum," +
            "quote2_num AS quote2Num," +
            "download_num AS downloadNum" +
            " FROM result_top WHERE task_id = #{task_id} and result_name= #{result_name} order by paper_count desc limit #{num}")
    List<ResultTop> findAllTopLimit(@Param("task_id") Integer taskId,@Param("result_name") String resultName,@Param("num") Integer num);

    @Select("SELECT " +
            "id," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "paper_count AS paperCount," +
            "quote_num AS quoteNum," +
            "quote2_num AS quote2Num," +
            "download_num AS downloadNum" +
            " FROM result_top WHERE task_id = #{task_id} and result_name= #{result_name} order by paper_count desc ")
    List<ResultTop> findAllTop(@Param("task_id") Integer taskId,@Param("result_name") String resultName);
}