package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.ResultDis;
import com.scnu.profile.mybatis.model.ResultTop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ResultDisMapper extends Mapper<ResultDis> {
    @Select("SELECT " +
            "id ," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "count " +
            "FROM result_dis WHERE task_id = #{task_id} and result_name= #{result_name} order by count desc limit #{num}")
    List<ResultDis> findAllDisLimit(@Param("task_id") Integer taskId, @Param("result_name") String resultName, @Param("num") Integer num);

    @Select("SELECT " +
            "id ," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "count " +
            "FROM result_dis WHERE task_id = #{task_id} and result_name= #{result_name} order by count desc")
    List<ResultDis> findAllDis(@Param("task_id") Integer taskId, @Param("result_name") String resultName);

}