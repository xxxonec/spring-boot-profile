package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Author;
import com.scnu.profile.mybatis.model.Paper;

import java.util.List;

public interface AuthorMapper extends Mapper<Author> {
    List<Paper> getAllPaper(String id);

    List<Paper> getTop5Paper(String id);
}