package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.mybatis.model.Unit;

import java.util.List;

public interface UnitMapper extends Mapper<Unit> {
    List<Paper> getAllPaper(String id);
    List<Unit> getAllNullAddress();
}