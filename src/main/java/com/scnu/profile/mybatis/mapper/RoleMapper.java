package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Role;

public interface RoleMapper extends Mapper<Role> {
    int insertAndGetId(Role role);
}