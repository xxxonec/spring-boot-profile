package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.User;

public interface UserMapper extends Mapper<User> {
    int insertAndGetId(User user);
}