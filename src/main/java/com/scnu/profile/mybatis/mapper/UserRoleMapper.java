package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.mybatis.model.UserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserRoleMapper extends Mapper<UserRole> {
    int deleteRole(@Param("userId") long userId, @Param("roleId") long roleId);
    int addRole(@Param("userId") long userId,@Param("roleId") long roleId);
    List<Role> getRoles(@Param("id") long userId);
    List<User> getUsers(@Param("id") long roleId);
}