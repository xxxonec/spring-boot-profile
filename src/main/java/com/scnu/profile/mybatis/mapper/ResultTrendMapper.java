package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.ResultTop;
import com.scnu.profile.mybatis.model.ResultTrend;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ResultTrendMapper extends Mapper<ResultTrend> {
    @Select("SELECT " +
            "id," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "r_year AS rYear," +
            "paper_count AS paperCount," +
            "quote_num AS quoteNum," +
            "quote2_num AS quote2Num," +
            "download_num AS downloadNum" +
            " FROM result_trend WHERE task_id = #{task_id} and result_name= #{result_name} order by r_year desc limit #{num}")
    List<ResultTrend> findAllTrendLimit(@Param("task_id") Integer taskId, @Param("result_name") String resultName, @Param("num") Integer YearNum);

    @Select("SELECT " +
            "id," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "r_year AS rYear," +
            "paper_count AS paperCount," +
            "quote_num AS quoteNum," +
            "quote2_num AS quote2Num," +
            "download_num AS downloadNum" +
            " FROM result_trend WHERE task_id = #{task_id} and result_name= #{result_name} order by r_year")
    List<ResultTrend> findAllTrend(@Param("task_id") Integer taskId, @Param("result_name") String resultName);

    @Select("SELECT " +
            "id," +
            "task_id AS taskId," +
            "result_name AS resultName," +
            "r_id AS rId," +
            "r_name AS rName," +
            "r_year AS rYear," +
            "j_name AS jName," +
            "paper_count AS paperCount," +
            "quote_num AS quoteNum," +
            "quote2_num AS quote2Num," +
            "download_num AS downloadNum" +
            " FROM result_trend WHERE task_id = #{task_id} and result_name= #{result_name} and r_id=#{r_id} and r_name=#{r_name} order by r_year")
    List<ResultTrend> findAuthorTrend(@Param("task_id") Integer taskId, @Param("result_name") String resultName,
                                      @Param("r_id") String rId,@Param("r_name") String rName);
}