package com.scnu.profile.mybatis.mapper;

import com.scnu.profile.core.mapper.Mapper;
import com.scnu.profile.mybatis.model.HIndex;

public interface HIndexMapper extends Mapper<HIndex> {
}