package com.scnu.profile.utils;

import com.scnu.profile.module.scholat.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by pcmmm on 17/7/4.
 */
@Lazy(false)
@Component
public class ServiceUtils {
    @Autowired
    private AuthorService authorService;
    @Autowired
    private PaperService paperService;
    @Autowired
    private JournalService journalService;
    @Autowired
    private UnitService unitService;
//    @Autowired
//    private LabelService labelService;

    private static ServiceUtils serviceUtils;

    @PostConstruct
    public void init() {
        serviceUtils = this;
        serviceUtils.authorService = this.authorService;
        serviceUtils.paperService = this.paperService;
        serviceUtils.journalService = this.journalService;
        serviceUtils.unitService = this.unitService;
//        serviceUtils.labelService = this.labelService;
    }

    public AuthorService getAuthorService() {
        return authorService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public static ServiceUtils getServiceUtils() {
        return serviceUtils;
    }

    public static void setServiceUtils(ServiceUtils serviceUtils) {
        ServiceUtils.serviceUtils = serviceUtils;
    }

    public PaperService getPaperService() {
        return paperService;
    }

    public void setPaperService(PaperService paperService) {
        this.paperService = paperService;
    }

    public JournalService getJournalService() {
        return journalService;
    }

    public void setJournalService(JournalService journalService) {
        this.journalService = journalService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

//    public LabelService getLabelService() {
//        return labelService;
//    }
//
//    public void setLabelService(LabelService labelService) {
//        this.labelService = labelService;
//    }

}
