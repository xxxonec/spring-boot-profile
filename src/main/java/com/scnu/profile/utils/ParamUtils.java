//package com.scnu.profile.utils;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.scnu.profile.config.ConfigurationManager;
//import com.scnu.profile.config.constants.Constants;
//import com.scnu.profile.exception.ParameterException;
//
///**
// * 参数工具类
// * @author Administrator
// *
// */
//public class ParamUtils {
//
//	/**
//	 * 从命令行参数中提取任务id
//	 * @param args 命令行参数
//	 * @return 任务id
//	 */
//	public static Long getTaskIdFromArgs(String[] args, String taskType) {
//		boolean local = ConfigurationManager.getBoolean(Constants.SPARK_LOCAL);
//
//		if(local) {
//			return ConfigurationManager.getLong(taskType);
//		} else {
//			try {
//				if(args != null && args.length > 0) {
//					return Long.valueOf(args[0]);
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//
//		return null;
//	}
//
//	/**
//	 * 从JSON对象中提取参数
//	 * @param jsonObject JSON对象
//	 * @return 参数
//	 */
//	public static String getParam(JSONObject jsonObject, String field) {
//		System.out.println(jsonObject);
//		//JSONArray jsonArray = jsonObject.getJSONArray(field);
//		//if(jsonArray != null && jsonArray.size() > 0) {
//		//	return jsonArray.getString(0);
//		//}
//		if(jsonObject.get(field)!= null){
//			//return jsonObject.get(field);
//		}
//		return null;
//	}
//
//	/**
//	 * 从JSON对象中提取参数
//	 * @param jsonObject JSON对象
//	 * @return 参数
//	 */
//	public static String getParamWithSplit(JSONObject jsonObject, String field) {
//		JSONArray jsonArray = jsonObject.getJSONArray(field);
//		String jsonString = "";
//		if(jsonArray != null && jsonArray.size() > 1) {
//			for (int i=0; i< jsonArray.size();i++){
//				if (i == jsonArray.size()-1){
//					jsonString += jsonArray.getString(i);
//				}else {
//					jsonString += jsonArray.getString(i) +",";
//				}
//			}
//			return jsonString;
//		}else if (jsonArray != null && jsonArray.size()==1){
//			return jsonArray.getString(0);
//		}else {
//			return null;
//		}
//	}
//
//	/**
//	 * 获得json某一关键字对应的一个值
//	 * 例如{"key":["value1"]},返回String类型的value1
//	 *
//	 * @param json
//	 * @param key
//	 * @return
//	 * @throws ParameterException
//	 */
//	public static String getSingleValue(org.json.JSONObject json, String key) throws ParameterException {
//		// 输入的key非法
//		if (key == null || key.equals("")) throw new ParameterException();
//		// 处理key不存在的异常
//		try {
//			org.json.JSONArray jsonArray = json.getJSONArray(key);
//			if (jsonArray != null && jsonArray.length() > 0) {
//				return jsonArray.getString(0);
//			}
//		} catch (Exception e) {
//			System.out.println(key + " doesn't exist.");
//		}
//
//		return null;
//	}
//
//	/**
//	 * 获得json某一关键字对应的多个值
//	 * 例如{"key":["value1","value2"]},返回String数组["value1,"value2"]
//	 *
//	 * @param json
//	 * @param key
//	 * @return
//	 * @throws ParameterException
//	 */
//	public static String[] getMultipleValues(org.json.JSONObject json, String key) throws ParameterException {
//		// 输入的key非法
//		if (key == null || key.equals("")) throw new ParameterException();
//		// 处理key不存在的异常
//		try {
//			org.json.JSONArray jsonArray = json.getJSONArray(key);
//			int jsonLength = jsonArray.length();
//			String[] jsonValues = new String[jsonLength];
//			for (int i = 0; i < jsonLength; i++) {
//				jsonValues[i] = jsonArray.getString(i);
//			}
//			return jsonValues;
//		} catch (Exception e) {
//			System.out.println(key + " doesn't exist.");
//		}
//
//		return null;
//	}
//
//}
