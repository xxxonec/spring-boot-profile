package com.scnu.profile.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by pcmmm on 17/7/4.
 */
public class AddressUtils {
    public static final String ADDRESS_API = "http://apis.map.qq.com/ws/place/v1/search?boundary=region(全国)" +
            "&keyword=%s&key=WEBBZ-TYQCS-TGHOM-6P3GO-PDKYZ-RQBC3";
    public static final String ADDRESS_DETAIL_API = "http://apis.map.qq.com/ws/place/v1/search?boundary=region(%s)" +
            "&keyword=%s&key=WEBBZ-TYQCS-TGHOM-6P3GO-PDKYZ-RQBC3";
    public static String getData(String unitName){
        String api = String.format(ADDRESS_API, unitName);
        System.out.println("start get data: "+ unitName);
        return post2api(api);
    }
    public static String getData(String unitName, String title){
        String api = String.format(ADDRESS_DETAIL_API, title, unitName);
        System.out.println("start get data time 2: "+ unitName);

        return post2api(api);
    }

    public static String post2api(String api){
        String result = "";
        BufferedReader in = null;
        try {
            //gb2312/utf-8
            URL realUrl = new URL(api);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    public static Map<String,String> parseData(String result, String unitName){
        //得到的json数据
        //System.out.println(result);
        //解析,
        JSONObject jsonObj = JSON.parseObject(result);
        JSONArray jarr =  jsonObj.getJSONArray("data");
        JSONArray cluster = jsonObj.getJSONArray("cluster");
        Map<String,String> map = new HashMap<>();
        if(!(cluster == null && jarr.isEmpty())){
            if(!jarr.isEmpty()&& jarr!=null){
                JSONObject j0 = (JSONObject)jarr.get(0);
                //输出该unit对应的地理位置
                String category = j0.get("category").toString();

                //经纬度
                JSONObject location = (JSONObject) j0.get("location");
                String lat = location.get("lat").toString();
                String lng = location.get("lng").toString();

                String address = j0.get("address").toString();

                JSONObject ad_info = (JSONObject) j0.get("ad_info");
                String province = ad_info.get("province").toString();
                String city = ad_info.get("city").toString();
                String district = ad_info.get("district").toString();
                map.put("unitName",unitName);
                map.put("category",category);
                map.put("address",address);
                map.put("lat",lat);
                map.put("lng",lng);
                map.put("province",province);
                map.put("city",city);
                map.put("district",district);
            }else if(!cluster.isEmpty() && cluster != null){
                JSONObject j0 = (JSONObject)cluster.get(0);
                String title = j0.get("title").toString();
                String new_result = getData(unitName, title);
                map = parseData(new_result, unitName);
            }else if(jarr.isEmpty()){
                System.out.println(unitName + " : parsed is empty!!!");
            }else if(jarr == null){
                System.out.println(unitName + " : parsed is null!!!");
            }else{
                System.out.println(" can't get the data of " + unitName);
            }
        }
        return map;
    }
    public static Map<String, String> getAddress(String unitName){
        String result = getData(unitName);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Map<String,String> map = parseData(result, unitName);
        return map;
    }

}
