package com.scnu.profile.utils;

/**
 * Created by pcmmm on 17/8/15.
 */
public class JsonResult<T> {

    private final boolean success;

    private final T data;

    private final String error;

    private final String msg;

    private JsonResult(JsonResultBuilder<T> builder) {
        this.success = builder.success;
        this.data = builder.data;
        this.error = builder.error;
        this.msg = builder.msg;
    }

    public static <T>JsonResult.JsonResultBuilder<T> builder(){
        return new JsonResultBuilder<>();
    }

    public boolean isSuccess() {
        return success;
    }

    public T getData() {
        return data;
    }

    public String getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "success=" + success +
                ", data=" + data +
                ", error='" + error + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    public static final class JsonResultBuilder<T> {

        private boolean success;

        private T data;

        private String error;

        private String msg;

        private JsonResultBuilder() {

        }


        public JsonResultBuilder error(String error) {
            this.error = error;
            this.success = false;
            return this;
        }

        public JsonResultBuilder data(T data,String msg) {
            this.msg = msg;
            this.data = data;
            this.success = true;
            return this;
        }

        public JsonResultBuilder data(T data) {
            this.data = data;
            this.success = true;
            return this;
        }

        public JsonResult build() {
            return new JsonResult<>(this);
        }
    }


}
