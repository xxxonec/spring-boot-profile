package com.scnu.profile.config.security;


import com.scnu.profile.mybatis.mapper.UserRoleMapper;
import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.mybatis.model.User;
import com.scnu.profile.module.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pcmmm on 17/8/15.
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 提供一种从用户名可以查到用户并返回的方法
     * @param username 帐号
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("'%s',该用户名未找到", username));
        } else {
            List<Role> roleList = userRoleMapper.getRoles(user.getId());
            user.setRoles(roleList);
            return JwtUserFactory.create(user);
        }
    }
}
