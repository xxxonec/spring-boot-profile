package com.scnu.profile.config.security;


import com.scnu.profile.mybatis.model.Role;
import com.scnu.profile.mybatis.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pcmmm on 17/8/15.
 */
final class JwtUserFactory {

    private JwtUserFactory() {
    }

    static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList())),
                user.getLastPwdReset()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> authorities) {
        return authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
