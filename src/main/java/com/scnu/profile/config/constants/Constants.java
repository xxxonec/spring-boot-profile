package com.scnu.profile.config.constants;

/**
 * 常量接口
 * Created by pcmmm on 17/7/4.
 */
public interface Constants {

    /**
     * 项目配置相关的常量
     */
    String JDBC_DRIVER = "jdbc.driver";
    String JDBC_DATASOURCE_SIZE = "jdbc.datasource.size";
    String JDBC_URL = "jdbc.url";
    String JDBC_USER = "jdbc.user";
    String JDBC_PASSWORD = "jdbc.password";
    String JDBC_URL_PROD = "jdbc.url.prod";
    String JDBC_USER_PROD = "jdbc.user.prod";
    String JDBC_PASSWORD_PROD = "jdbc.password.prod";
    String SPARK_LOCAL = "spark.local";
    String SPARK_LOCAL_SESSION_DATA_PATH = "spark.data.session.path";
    String SPARK_LOCAL_USER_DATA_PATH = "spark.data.user.path";
    String SPARK_MASTER = "local";
    String SPARK_LOCAL_TASKID_SESSION = "spark.local.taskid.session";
    String SPARK_LOCAL_TASKID_PAGE = "spark.local.taskid.page";
    String SPARK_LOCAL_TASKID_PRODUCT = "spark.local.taskid.product";
    String KAFKA_METADATA_BROKER_LIST = "kafka.metadata.broker.list";
    String KAFKA_TOPICS = "kafka.topics";
    String LOCAL_SESSION_DATA_PATH = "spark.data.session.path";
    String LOCAL_USER_DATA_PATH = "spark.data.user.path";
    String LOCAL_PRODUCT_DATA_PATH = "spark.data.user.path";
    String LOCAL_AUTHOR_DATA_PATH = "spark.data.author.path";
    String LOCAL_PAPER_DATA_PATH = "spark.data.paper.path";
    String LOCAL_UNIT_DATA_PATH = "spark.data.unit.path";
    String LOCAL_JOURNAL_DATA_PATH = "spark.data.journal.path";
    /**
     * Spark作业相关的常量
     */
    String SPARK_APP_NAME_SESSION = "ScholarProfile";



    /**
     * 字符串分隔符
     */
    String VALUE_SEPARATOR = "|";
    String REGULAR_VALUE_SEPARATOR = "\\|";

    /**
     * SparkSQL
     */
    String TABLE_USER_VISIT_ACTION = "user_visit_action";
    String TABLE_USER_INFO = "user_info";

    String TABLE_PAPER_INFO = "paper_info";
    String TABLE_UNIT_INFO = "unit_info";
    String TABLE_AUTHOR_INFO = "author_info";
    String TABLE_JOURNAL_INFO = "journal_info";

    String SESSION_COUNT = "session_count";

    String TIME_PERIOD_1s_3s = "1s_3s";
    String TIME_PERIOD_4s_6s = "4s_6s";
    String TIME_PERIOD_7s_9s = "7s_9s";
    String TIME_PERIOD_10s_30s = "10s_30s";
    String TIME_PERIOD_30s_60s = "30s_60s";
    String TIME_PERIOD_1m_3m = "1m_3m";
    String TIME_PERIOD_3m_10m = "3m_10m";
    String TIME_PERIOD_10m_30m = "10m_30m";
    String TIME_PERIOD_30m = "30m";

    String STEP_PERIOD_1_3 = "1_3";
    String STEP_PERIOD_4_6 = "4_6";
    String STEP_PERIOD_7_9 = "7_9";
    String STEP_PERIOD_10_30 = "10_30";
    String STEP_PERIOD_30_60 = "30_60";
    String STEP_PERIOD_60 = "60";






    //AGE
    //String AGE_UNDER_20 = "20";
    /**
     * 任务相关的常量
     */
    String PARAM_START_DATE = "startDate";
    String PARAM_END_DATE = "endDate";
    String PARAM_START_AGE = "startAge";
    String PARAM_END_AGE = "endAge";
    String PARAM_PROFESSIONALS = "professionals";
    String PARAM_CITYS = "cities";
    String PARAM_SEX = "sex";
    String PARAM_KEYWORDS = "keywords";
    String PARAM_SEARCH_WORDS = "keywords";
    String PARAM_CATEGORY_IDS = "categoryIds";
    String PARAM_TARGET_PAGE_FLOW = "targetPageFlow";

}
