package com.scnu.profile.core.result;

/**
 * 响应结果生成工具
 */
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMsg(DEFAULT_SUCCESS_MESSAGE)
                .setSuccess(true);
    }

    public static Result genSuccessResult(Object data) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMsg(DEFAULT_SUCCESS_MESSAGE)
                .setData(data)
                .setSuccess(true);
    }
    public static Result genSuccessResult(Object data,String msg) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMsg(msg)
                .setData(data)
                .setSuccess(true);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCode.FAIL)
                .setMsg(message)
                .setSuccess(false);
    }
}
