package com.scnu.profile.core;

import com.scnu.profile.mybatis.model.Author;
import com.scnu.profile.mybatis.model.Journal;
import com.scnu.profile.mybatis.model.Paper;
import com.scnu.profile.mybatis.model.Unit;
import com.scnu.profile.utils.ServiceUtils;

import java.io.*;
import java.util.List;

/**
 * Created by pcmmm on 17/7/5.
 */
public abstract class Convert2txt {

    public static void convert() {
        List<Paper> paperList  =ServiceUtils.getServiceUtils().getPaperService().findAll();
        List<Author> authorList  =ServiceUtils.getServiceUtils().getAuthorService().findAll();
        List<Unit> unitList  =ServiceUtils.getServiceUtils().getUnitService().findAll();
        List<Journal> journalList  =ServiceUtils.getServiceUtils().getJournalService().findAll();
        try {
            FileWriter paperWriter = new FileWriter("data/paper.txt",true);
            FileWriter authorWriter = new FileWriter("data/author.txt",true);
            FileWriter unitWriter = new FileWriter("data/unit.txt",true);
            FileWriter journalWriter = new FileWriter("data/journal.txt",true);
            for(int i = 0;i<paperList.size();i++) {
                paperWriter.write(paperList.get(i).toString());
            }
            for(int i = 0;i<authorList.size();i++) {
                authorWriter.write(authorList.get(i).toString());
            }
            for(int i = 0;i<unitList.size();i++) {
                unitWriter.write(unitList.get(i).toString());
            }
            for(int i = 0;i<journalList.size();i++) {
                journalWriter.write(journalList.get(i).toString());
            }
            paperWriter.close();
            authorWriter.close();
            unitWriter.close();
            journalWriter.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
